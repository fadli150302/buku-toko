import 'package:buku_toko/service/Database/database.dart';
import 'package:buku_toko/util/locator.dart';

import '../../model/my_model.dart';

class TransactionLocalDataSource {
  Future<Transaction?> getTransaction(int id) async {
    return locator<ObjectBoxDatabase>().getData<Transaction>(id);
  }

  Future<List<Transaction>> getAllTransaction() async {
    return locator<ObjectBoxDatabase>().getAll<Transaction>();
  }

  Future<void> addTransaction(Transaction transaction) async {
    await locator<ObjectBoxDatabase>().createData<Transaction>(transaction);
  }

  Future<void> deleteTransaction(int id) async {
    await locator<ObjectBoxDatabase>().deleteData<Transaction>(id);
  }
}
