import 'package:buku_toko/service/Database/database.dart';
import 'package:buku_toko/util/locator.dart';

import '../../model/my_model.dart';

class FinanceLocalDataSource {
  Future<List<Finance>> getAllFinance() async {
    return locator<ObjectBoxDatabase>().getAll<Finance>();
  }

  Future<Finance?> getFinance(int id) async {
    return locator<ObjectBoxDatabase>().getData<Finance>(id);
  }

  Future<void> addFinance(Finance finance) async {
    await locator<ObjectBoxDatabase>().createData<Finance>(finance);
  }

  Future<void> deleteFinance(int id) async {
    await locator<ObjectBoxDatabase>().deleteData<Finance>(id);
  }

  Future<void> updateFinance(Finance finance) async {
    await locator<ObjectBoxDatabase>().updateData<Finance>(finance, finance.id);
  }
}
