import 'package:buku_toko/data/model/my_model.dart';
import 'package:buku_toko/service/Database/database.dart';
import 'package:buku_toko/util/locator.dart';

class GoodsLocalDataSource {
  Future<List<Goods>> getAllGoods() async {
    final listGoods = await locator<ObjectBoxDatabase>().getAll<Goods>();
    return listGoods;
  }

  Future<Goods?> getGoods(int id) async {
    return locator<ObjectBoxDatabase>().getData<Goods>(id);
  }

  Future<void> addGoods(Goods goods) async {
    await locator<ObjectBoxDatabase>().createData<Goods>(goods);
  }

  Future<void> deleteGoods(int id) async {
    await locator<ObjectBoxDatabase>().deleteData<Goods>(id);
  }

  Future<void> updateGoods(Goods goods) async {
    await locator<ObjectBoxDatabase>().updateData<Goods>(goods, goods.id);
  }

  Future<void> addStockGoods(int total, int id) async {
    final goods = await locator<ObjectBoxDatabase>().getData<Goods>(id);
    if (goods == null) return;
    final now = DateTime.now().toString();
    await locator<ObjectBoxDatabase>().updateData<Goods>(
      goods.goodsWithNewStock(goods.stock + total, now),
      goods.id,
    );
  }

  Future<void> reduceStockGoods(int total, int id) async {
    final goods = await locator<ObjectBoxDatabase>().getData<Goods>(id);
    if (goods == null) return;
    final now = DateTime.now().toString();
    await locator<ObjectBoxDatabase>().updateData<Goods>(
      goods.goodsWithNewStock(goods.stock - total, now),
      goods.id,
    );
  }
}
