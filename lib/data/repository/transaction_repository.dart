import 'package:buku_toko/util/request.dart';

import '../model/massage_response.dart';
import '../model/my_model.dart';
import '../source/local/transaction_local_data_source_m.dart';

abstract class TransactionRepository {
  Future<List<Transaction>> getAllTransaction();

  Future<Transaction?> getTransaction(int id);

  Future<MessageResponse> addTransaction(Transaction transaction);

  Future<MessageResponse> deleteTransaction(int id);
}

class TransactionRepositoryImpl extends TransactionRepository {
  final TransactionLocalDataSource _transactionLocalDataSource;
  TransactionRepositoryImpl(this._transactionLocalDataSource);
  @override
  Future<MessageResponse> addTransaction(Transaction transaction) {
    return RequestLocal<MessageResponse>().requestLocal(
      massage: 'addTransaction',
      computation: () async {
        await _transactionLocalDataSource.addTransaction(transaction);
        return MessageResponse(status: 'Success');
      },
    );
  }

  @override
  Future<MessageResponse> deleteTransaction(int id) {
    return RequestLocal<MessageResponse>().requestLocal(
      massage: 'deleteTransaction',
      computation: () async {
        await _transactionLocalDataSource.deleteTransaction(id);
        return MessageResponse(status: 'Success');
      },
    );
  }

  @override
  Future<Transaction?> getTransaction(int id) {
    return RequestLocal<Transaction?>().requestLocal(
      massage: 'getTransaction',
      computation: () async {
        final data = await _transactionLocalDataSource.getTransaction(id);
        return data;
      },
    );
  }

  @override
  Future<List<Transaction>> getAllTransaction() {
    return RequestLocal<List<Transaction>>().requestLocal(
      massage: 'getAllTransaction',
      computation: () async {
        final data = await _transactionLocalDataSource.getAllTransaction();
        return data;
      },
    );
  }
}
