import 'package:buku_toko/util/request.dart';

import '../model/massage_response.dart';
import '../model/my_model.dart';
import '../source/local/finance_local_data_source.dart';

abstract class FinanceRepository {
  Future<List<Finance>> getAllFinance();

  Future<Finance?> getFinance(int id);

  Future<MessageResponse> addFinance(Finance finance);

  Future<MessageResponse> deleteFinance(int id);

  Future<MessageResponse> updateFinance(Finance finance);
}

class FinanceRepositoryImpl extends FinanceRepository {
  final FinanceLocalDataSource _financeLocalDataSource;
  FinanceRepositoryImpl(this._financeLocalDataSource);
  @override
  Future<MessageResponse> addFinance(Finance finance) {
    return RequestLocal<MessageResponse>().requestLocal(
      massage: 'addFinance',
      computation: () async {
        await _financeLocalDataSource.addFinance(finance);
        return MessageResponse(status: 'Success');
      },
    );
  }

  @override
  Future<MessageResponse> deleteFinance(int id) {
    return RequestLocal<MessageResponse>().requestLocal(
      massage: 'deleteFinance',
      computation: () async {
        await _financeLocalDataSource.deleteFinance(id);
        return MessageResponse(status: 'Success');
      },
    );
  }

  @override
  Future<List<Finance>> getAllFinance() {
    return RequestLocal<List<Finance>>().requestLocal(
      massage: 'getAllFinance',
      computation: () async {
        final data = await _financeLocalDataSource.getAllFinance();
        return data;
      },
    );
  }

  @override
  Future<Finance?> getFinance(int id) {
    return RequestLocal<Finance?>().requestLocal(
      massage: 'getFinance',
      computation: () async {
        final data = await _financeLocalDataSource.getFinance(id);
        return data;
      },
    );
  }

  @override
  Future<MessageResponse> updateFinance(Finance finance) {
    return RequestLocal<MessageResponse>().requestLocal(
      massage: 'updateFinance',
      computation: () async {
        await _financeLocalDataSource.updateFinance(finance);
        return MessageResponse(status: 'Success');
      },
    );
  }
}
