import 'package:buku_toko/data/source/local/goods_local_data_source.dart';
import 'package:buku_toko/util/request.dart';

import '../model/massage_response.dart';
import '../model/my_model.dart';

abstract class GoodsRepository {
  Future<List<Goods>> getAllGoods();

  Future<Goods?> getGoods(int id);

  Future<MessageResponse> addGoods(Goods goods);

  Future<MessageResponse> deleteGoods(int id);

  Future<MessageResponse> updateGoods(Goods goods);

  Future<MessageResponse> addStockGoods(int total, int id);

  Future<MessageResponse> reduceStockGoods(int total, int id);
}

class GoodsRepositoryImpl extends GoodsRepository {
  final GoodsLocalDataSource _goodsLocalDataSource;
  GoodsRepositoryImpl(this._goodsLocalDataSource);
  @override
  Future<MessageResponse> addGoods(Goods goods) {
    return RequestLocal<MessageResponse>().requestLocal(
      massage: 'addGoods',
      computation: () async {
        await _goodsLocalDataSource.addGoods(goods);
        return MessageResponse(status: 'Success');
      },
    );
  }

  @override
  Future<MessageResponse> deleteGoods(int id) {
    return RequestLocal<MessageResponse>().requestLocal(
      massage: 'deleteGoods',
      computation: () async {
        await _goodsLocalDataSource.deleteGoods(id);
        return MessageResponse(status: 'Success');
      },
    );
  }

  @override
  Future<List<Goods>> getAllGoods() {
    return RequestLocal<List<Goods>>().requestLocal(
      massage: 'getAllGoods',
      computation: () async {
        final data = await _goodsLocalDataSource.getAllGoods();
        return data;
      },
    );
  }

  @override
  Future<Goods?> getGoods(int id) {
    return RequestLocal<Goods?>().requestLocal(
      massage: 'getGoods',
      computation: () async {
        final data = await _goodsLocalDataSource.getGoods(id);
        return data;
      },
    );
  }

  @override
  Future<MessageResponse> updateGoods(Goods goods) {
    return RequestLocal<MessageResponse>().requestLocal(
      massage: 'updateGoods',
      computation: () async {
        await _goodsLocalDataSource.updateGoods(goods);
        return MessageResponse(status: 'Success');
      },
    );
  }

  @override
  Future<MessageResponse> addStockGoods(int total, int id) {
    return RequestLocal<MessageResponse>().requestLocal(
      massage: 'addStockGoods',
      computation: () async {
        await _goodsLocalDataSource.addStockGoods(total, id);
        return MessageResponse(status: 'Success');
      },
    );
  }

  @override
  Future<MessageResponse> reduceStockGoods(int total, int id) {
    return RequestLocal<MessageResponse>().requestLocal(
      massage: 'reduceStockGoods',
      computation: () async {
        await _goodsLocalDataSource.reduceStockGoods(total, id);
        return MessageResponse(status: 'Success');
      },
    );
  }
}
