import 'dart:convert';

import 'package:objectbox/objectbox.dart';

@Entity()
class Goods {
  @Id(assignable: true)
  int id;
  final String name;
  final String createdAt;
  final String updatedAt;
  final int stock;
  final String barCode;
  final int buyPrice;
  final int sellPrice;
  final int discount;
  final String typeGoods;

  Goods goodsWithNewStock(int total, String? updated) {
    return Goods(
      id: id,
      name: name,
      barCode: barCode,
      buyPrice: buyPrice,
      createdAt: createdAt,
      discount: discount,
      sellPrice: sellPrice,
      stock: total,
      typeGoods: typeGoods,
      updatedAt: updated ?? updatedAt,
    );
  }

  String toTransactionModel(int total) {
    return json.encode({
      'id': id,
      'name': name,
      'stock': total,
      'buy_price': buyPrice,
      'sell_price': sellPrice,
      'discount': discount,
      'type_goods': typeGoods,
      'created_at': createdAt,
      'updated_at': updatedAt,
      'bar_code': barCode,
    });
  }

  factory Goods.fromTransactionModel(String value) {
    final map = json.decode(value);
    return Goods(
      id: map['id'],
      name: map['name'],
      barCode: map['bar_code'],
      buyPrice: map['buy_price'],
      createdAt: map['created_at'],
      discount: map['discount'],
      sellPrice: map['sell_price'],
      stock: map['stock'],
      typeGoods: map['type_goods'],
      updatedAt: map['updated_at'],
    );
  }

  Goods({
    this.id = 0,
    required this.name,
    required this.barCode,
    required this.buyPrice,
    required this.createdAt,
    required this.discount,
    required this.sellPrice,
    required this.stock,
    required this.typeGoods,
    required this.updatedAt,
  });
}

@Entity()
class Finance {
  @Id(assignable: true)
  int id;
  final int cash;
  final bool isIn;
  final String createdAt;
  final String updatedAt;
  final String description;

  Finance({
    this.id = 0,
    required this.cash,
    required this.createdAt,
    required this.isIn,
    required this.updatedAt,
    required this.description,
  });
}

@Entity()
class Transaction {
  @Id(assignable: true)
  int id;

  final int financeId;
  final String createdAt;
  final String updatedAt;
  final int totalPrice;
  final int totalProfit;
  final int totalItem;
  final int guestPay;
  final List<String> listGoods;

  List<Goods> getListGoods() {
    final List<Goods> list = [];
    for (var element in listGoods) {
      list.add(Goods.fromTransactionModel(element));
    }
    return list;
  }

  Transaction({
    this.id = 0,
    required this.financeId,
    required this.createdAt,
    required this.guestPay,
    required this.totalItem,
    required this.totalPrice,
    required this.listGoods,
    required this.totalProfit,
    required this.updatedAt,
  });
}
