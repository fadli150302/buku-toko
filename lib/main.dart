import 'package:buku_toko/util/const.dart';
import 'package:buku_toko/view/finance/bloc/finance_bloc.dart';
import 'package:buku_toko/view/good/bloc/goods_bloc.dart';
import 'package:buku_toko/view/splash/splash_page.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:intl/date_symbol_data_local.dart';

import 'data/repository/finance_repository.dart';
import 'data/repository/goods_repository.dart';
import 'data/repository/transaction_repository.dart';
import 'util/locator.dart';
import 'util/route.dart';
import 'view/transaction/bloc/transaction_bloc.dart';

void main() async {
  WidgetsFlutterBinding.ensureInitialized();
  await initServiceLocator();
  await initializeDateFormatting('id_ID', null);
  runApp(const MyApp());
}

class MyApp extends StatelessWidget {
  const MyApp({super.key});

  @override
  Widget build(BuildContext context) {
    SystemChrome.setPreferredOrientations([
      DeviceOrientation.portraitUp,
      DeviceOrientation.portraitDown,
    ]);

    final router = AppRouter();
    return MultiBlocProvider(
      providers: [
        BlocProvider(
          create: (context) => FinanceBloc(locator<FinanceRepositoryImpl>()),
        ),
        BlocProvider(
          create: (context) => GoodsBloc(locator<GoodsRepositoryImpl>()),
        ),
        BlocProvider(
          create: (context) =>
              TransactionBloc(locator<TransactionRepositoryImpl>()),
        ),
      ],
      child: MaterialApp(
        title: appName,
        onGenerateRoute: router.routes,
        navigatorKey: locator<NavigationService>().navigatorKey,
        home: const SplashPage(),
      ),
    );
  }
}
