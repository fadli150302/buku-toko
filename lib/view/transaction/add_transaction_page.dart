import 'package:buku_toko/data/model/my_model.dart';
import 'package:buku_toko/util/loading.dart';
import 'package:buku_toko/util/toast.dart';
import 'package:buku_toko/view/good/list_goods_page.dart';
import 'package:buku_toko/view/transaction/widget/transcation_goods_item.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

import '../../theme/theme.dart';
import '../../util/app_config.dart';
import '../../util/const.dart';
import '../../util/date.dart';
import '../../util/locator.dart';
import '../../util/route.dart';
import '../../widget/custom_container_widget.dart';
import '../../widget/pop_widget.dart';
import 'bloc/manipulate_transaction_bloc.dart';
import 'bloc/transaction_bloc.dart';
import 'cubit/add_goods_in_transaction_cubit.dart';

class AddTransactionPage extends StatefulWidget {
  const AddTransactionPage({this.transaction, super.key});
  static const route = '/add_transaction_page';
  final Transaction? transaction;

  @override
  State<AddTransactionPage> createState() => _AddTransactionPageState();
}

class _AddTransactionPageState extends State<AddTransactionPage> {
  TextEditingController totalBayarController = TextEditingController();
  TextEditingController totalItemController = TextEditingController();
  TextEditingController totalPriceController = TextEditingController();
  TextEditingController totalProfitController = TextEditingController();
  TextEditingController tanggalController = TextEditingController();

  late AddGoodsInTransactionCubit addGoodsInTransactionCubit;

  @override
  void initState() {
    super.initState();
    totalBayarController.text =
        widget.transaction?.guestPay.toString() ?? empty;
    totalItemController.text =
        widget.transaction?.totalItem.toString() ?? empty;
    totalPriceController.text =
        widget.transaction?.totalPrice.toString() ?? empty;
    totalProfitController.text =
        widget.transaction?.totalProfit.toString() ?? empty;
    tanggalController.text =
        locator<MyDate>().getDateWithInputStringIndonesiaFormatJustDate(
      widget.transaction?.createdAt ?? DateTime.now().toString(),
    );

    addGoodsInTransactionCubit =
        BlocProvider.of<AddGoodsInTransactionCubit>(context);

    if (widget.transaction != null) {
      addGoodsInTransactionCubit.setListGoods(
        widget.transaction!
            .getListGoods()
            .map((e) => TransactionGoodsItem(
                  goods: e,
                  isTransactionClear: true,
                  total: ValueNotifier(e.stock),
                  callback: () {
                    addGoodsInTransactionCubit.removeGoods(e);
                  },
                ))
            .toList(),
      );
    }
  }

  refreshText(List<TransactionGoodsItem> state) async {
    int totalItem = 0;
    int totalPrice = 0;
    int totalProfit = 0;
    for (var element in state) {
      totalItem += element.total.value;
      totalPrice += element.goods.sellPrice * element.total.value;
      totalProfit += element.goods.sellPrice * element.total.value -
          element.goods.buyPrice * element.total.value;
    }
    totalItemController.text = totalItem.toString();
    totalPriceController.text = totalPrice.toString();
    totalProfitController.text = totalProfit.toString();
  }

  @override
  Widget build(BuildContext context) {
    return BlocListener<ManipulateTransactionBloc, ManipulateTransactionState>(
      listener: (context, state) {
        LoadingOverlay.dismiss();
        if (state is ManipulateTransactionSuccess) {
          BlocProvider.of<TransactionBloc>(context).add(GetTransactionEvent());
          Navigator.pop(context);
          Toast.showSuccess('Success', context);
        } else if (state is ManipulateTransactionError) {
          Toast.showError(state.message, context);
        } else {
          LoadingOverlay.show(context);
        }
      },
      child: Scaffold(
        backgroundColor: locator<AppConfig>().color().secondaryAccent,
        body: SizedBox(
          height: MediaQuery.of(context).size.height,
          width: MediaQuery.of(context).size.width,
          child: SingleChildScrollView(
            child: Padding(
              padding: const EdgeInsets.all(20),
              child: SafeArea(
                child: Column(
                  children: [
                    // HEADER
                    CustomContainerWidget(
                      containerType: CustomContainerType.container_312_x_119,
                      child: Padding(
                        padding: const EdgeInsets.all(20),
                        child: Column(
                          children: [
                            Row(
                              children: [
                                GestureDetector(
                                  onTap: () {
                                    Navigator.pop(context);
                                  },
                                  child: const Icon(
                                    Icons.arrow_back,
                                    color: white,
                                  ),
                                ),
                                const SizedBox(
                                  width: 16,
                                ),
                                Text(
                                  widget.transaction != null
                                      ? 'Edit Transaksi'
                                      : 'Tambah Transaksi',
                                  style: poppinsTextStyle.copyWith(
                                    fontWeight: semiBold,
                                    fontSize: 16,
                                    color: white,
                                  ),
                                ),
                                const Spacer(),
                                widget.transaction != null
                                    ? PopWidget(
                                        scale: 0.04,
                                        onTap: () {
                                          BlocProvider.of<
                                              ManipulateTransactionBloc>(
                                            context,
                                          ).add(DeleteTransactionEvent(
                                            widget.transaction!,
                                          ));
                                        },
                                        child: iconDelete,
                                      )
                                    : PopWidget(
                                        onTap: () {},
                                        child: iconAdd,
                                      ),
                              ],
                            ),
                          ],
                        ),
                      ),
                    ),
                    // BODY
                    Padding(
                      padding: const EdgeInsets.symmetric(vertical: 14),
                      child: CustomContainerWidget(
                        containerType: CustomContainerType.container_312_x_445,
                        child: Padding(
                          padding: const EdgeInsets.symmetric(horizontal: 20),
                          child: Column(
                            crossAxisAlignment: CrossAxisAlignment.start,
                            children: [
                              const SizedBox(
                                height: 20,
                              ),
                              // TOTAL BAYAR
                              Text(
                                'List Barang',
                                style: poppinsTextStyle.copyWith(
                                  color: white,
                                  fontWeight: medium,
                                  fontSize: 12,
                                ),
                              ),
                              const SizedBox(
                                height: 6,
                              ),
                              BlocConsumer<AddGoodsInTransactionCubit,
                                  AddGoodsInTransactionState>(
                                buildWhen: (previous, current) => true,
                                listenWhen: (previous, current) => true,
                                listener: (context, state) async {
                                  refreshText(state.listGoods);
                                },
                                builder: (context, state) {
                                  return Column(
                                    children: [
                                      ...state.listGoods,
                                      if (widget.transaction == null)
                                        PopWidget(
                                          onTap: () async {
                                            final result = await locator<
                                                    NavigationService>()
                                                .navigatorKey
                                                .currentState
                                                ?.pushNamed(
                                                  ListGoodsPage.route,
                                                  arguments: true,
                                                );
                                            // CHECK ITEM ADDED OR NOT
                                            if (result is Goods) {
                                              for (var element
                                                  in state.listGoods) {
                                                if (element.goods.id ==
                                                    result.id) {
                                                  if (mounted) {
                                                    Toast.showError(
                                                      'Item Sudah Ditambahkan',
                                                      context,
                                                    );
                                                  }
                                                  return;
                                                }
                                              }
                                              addGoodsInTransactionCubit
                                                  .addGoods(
                                                TransactionGoodsItem(
                                                  goods: result,
                                                  isTransactionClear: false,
                                                  total: ValueNotifier(1),
                                                  callback: () {
                                                    addGoodsInTransactionCubit
                                                        .removeGoods(result);
                                                  },
                                                ),
                                              );
                                            }
                                          },
                                          child: Container(
                                            height: 60,
                                            padding: const EdgeInsets.symmetric(
                                              horizontal: 20,
                                            ),
                                            decoration: BoxDecoration(
                                              color: white,
                                              borderRadius:
                                                  BorderRadius.circular(12),
                                            ),
                                            child: Center(
                                              child: Text(
                                                'Pilih Barang +',
                                                style:
                                                    poppinsTextStyle.copyWith(
                                                  fontWeight: semiBold,
                                                  fontSize: 14,
                                                  color: locator<AppConfig>()
                                                      .color()
                                                      .primary,
                                                ),
                                              ),
                                            ),
                                          ),
                                        ),
                                    ],
                                  );
                                },
                              ),
                              const SizedBox(
                                height: 14,
                              ),
                              // TOTAL BAYAR
                              Text(
                                'Total Bayar (Konsumen)',
                                style: poppinsTextStyle.copyWith(
                                  color: white,
                                  fontWeight: medium,
                                  fontSize: 12,
                                ),
                              ),
                              const SizedBox(
                                height: 6,
                              ),
                              Container(
                                padding:
                                    const EdgeInsets.symmetric(horizontal: 20),
                                decoration: BoxDecoration(
                                  color: white,
                                  borderRadius: BorderRadius.circular(12),
                                ),
                                child: TextField(
                                  controller: totalBayarController,
                                  style: poppinsTextStyle.copyWith(
                                    fontWeight: semiBold,
                                    fontSize: 14,
                                    color: locator<AppConfig>().color().primary,
                                  ),
                                  keyboardType: TextInputType.number,
                                  textAlign: TextAlign.start,
                                  decoration: InputDecoration(
                                    border: InputBorder.none,
                                    hintStyle: poppinsTextStyle.copyWith(
                                      fontWeight: semiBold,
                                      fontSize: 14,
                                      color: black12,
                                    ),
                                    hintText: 'Total Bayar',
                                  ),
                                ),
                              ),
                              const SizedBox(
                                height: 14,
                              ),
                              // TOTAL ITEM
                              Text(
                                'Total Item',
                                style: poppinsTextStyle.copyWith(
                                  color: white,
                                  fontWeight: medium,
                                  fontSize: 12,
                                ),
                              ),
                              const SizedBox(
                                height: 6,
                              ),
                              Container(
                                padding:
                                    const EdgeInsets.symmetric(horizontal: 20),
                                decoration: BoxDecoration(
                                  color: white,
                                  borderRadius: BorderRadius.circular(12),
                                ),
                                child: TextField(
                                  controller: totalItemController,
                                  style: poppinsTextStyle.copyWith(
                                    fontWeight: medium,
                                    fontSize: 18,
                                    color: warningMain,
                                  ),
                                  enabled: false,
                                  textAlign: TextAlign.end,
                                  decoration: InputDecoration(
                                    border: InputBorder.none,
                                    hintStyle: poppinsTextStyle.copyWith(
                                      fontWeight: semiBold,
                                      fontSize: 14,
                                      color: black12,
                                    ),
                                    hintText: 'Total Item',
                                  ),
                                ),
                              ),
                              const SizedBox(
                                height: 14,
                              ),
                              // TOTAL HARGA
                              Text(
                                'Total Harga',
                                style: poppinsTextStyle.copyWith(
                                  color: white,
                                  fontWeight: medium,
                                  fontSize: 12,
                                ),
                              ),
                              const SizedBox(
                                height: 6,
                              ),
                              Container(
                                padding:
                                    const EdgeInsets.symmetric(horizontal: 20),
                                decoration: BoxDecoration(
                                  color: white,
                                  borderRadius: BorderRadius.circular(12),
                                ),
                                child: TextField(
                                  controller: totalPriceController,
                                  style: poppinsTextStyle.copyWith(
                                    fontWeight: medium,
                                    fontSize: 18,
                                    color: successMain,
                                  ),
                                  enabled: false,
                                  textAlign: TextAlign.end,
                                  decoration: InputDecoration(
                                    border: InputBorder.none,
                                    hintStyle: poppinsTextStyle.copyWith(
                                      fontWeight: semiBold,
                                      fontSize: 14,
                                      color: black12,
                                    ),
                                    hintText: 'Total Harga',
                                  ),
                                ),
                              ),
                              const SizedBox(
                                height: 14,
                              ),
                              // TOTAL PROFIT
                              Text(
                                'Total Profit',
                                style: poppinsTextStyle.copyWith(
                                  color: white,
                                  fontWeight: medium,
                                  fontSize: 12,
                                ),
                              ),
                              const SizedBox(
                                height: 6,
                              ),
                              Container(
                                padding:
                                    const EdgeInsets.symmetric(horizontal: 20),
                                decoration: BoxDecoration(
                                  color: white,
                                  borderRadius: BorderRadius.circular(12),
                                ),
                                child: TextField(
                                  controller: totalProfitController,
                                  style: poppinsTextStyle.copyWith(
                                    fontWeight: medium,
                                    fontSize: 18,
                                    color: locator<AppConfig>().color().primary,
                                  ),
                                  textAlign: TextAlign.end,
                                  enabled: false,
                                  decoration: InputDecoration(
                                    border: InputBorder.none,
                                    hintStyle: poppinsTextStyle.copyWith(
                                      fontWeight: semiBold,
                                      fontSize: 14,
                                      color: black12,
                                    ),
                                    hintText: 'Total Profit',
                                  ),
                                ),
                              ),
                              const SizedBox(
                                height: 14,
                              ),
                              // TANGGAL
                              Text(
                                'Tanggal',
                                style: poppinsTextStyle.copyWith(
                                  color: white,
                                  fontWeight: medium,
                                  fontSize: 12,
                                ),
                              ),
                              const SizedBox(
                                height: 6,
                              ),
                              Container(
                                padding:
                                    const EdgeInsets.symmetric(horizontal: 20),
                                decoration: BoxDecoration(
                                  color: white,
                                  borderRadius: BorderRadius.circular(12),
                                ),
                                child: TextField(
                                  controller: tanggalController,
                                  style: poppinsTextStyle.copyWith(
                                    fontWeight: medium,
                                    fontSize: 18,
                                    color: locator<AppConfig>().color().primary,
                                  ),
                                  enabled: false,
                                  textAlign: TextAlign.start,
                                  decoration: InputDecoration(
                                    border: InputBorder.none,
                                    hintStyle: poppinsTextStyle.copyWith(
                                      fontWeight: semiBold,
                                      fontSize: 14,
                                      color: black12,
                                    ),
                                    hintText: 'Tanggal',
                                  ),
                                ),
                              ),
                              const SizedBox(
                                height: 20,
                              ),
                            ],
                          ),
                        ),
                      ),
                    ),
                    // FOOTER
                    BlocBuilder<AddGoodsInTransactionCubit,
                        AddGoodsInTransactionState>(
                      buildWhen: (previous, current) => true,
                      builder: (context, state) {
                        if (widget.transaction == null) {
                          return PopWidget(
                            scale: 0.02,
                            onTap: () {
                              Map<Goods, int> map = {};
                              for (var element in state.listGoods) {
                                map.addAll(
                                  {element.goods: element.total.value},
                                );
                              }

                              if (widget.transaction == null) {
                                BlocProvider.of<ManipulateTransactionBloc>(
                                  context,
                                ).add(AddTransactionEvent(
                                  guestPay:
                                      int.tryParse(totalBayarController.text) ??
                                          0,
                                  totalProfit: int.tryParse(
                                        totalProfitController.text,
                                      ) ??
                                      0,
                                  totalPrice:
                                      int.tryParse(totalPriceController.text) ??
                                          0,
                                  totalItem:
                                      int.tryParse(totalItemController.text) ??
                                          0,
                                  map: map,
                                ));
                              }
                            },
                            child: CustomContainerWidget(
                              containerType:
                                  CustomContainerType.container_312_x_67,
                              child: Padding(
                                padding: const EdgeInsets.all(15),
                                child: Container(
                                  padding: const EdgeInsets.all(12),
                                  decoration: const BoxDecoration(
                                    color: white,
                                    borderRadius:
                                        BorderRadius.all(Radius.circular(12)),
                                  ),
                                  child: Row(
                                    mainAxisAlignment: MainAxisAlignment.center,
                                    children: [
                                      Text(
                                        'Simpan',
                                        style: poppinsTextStyle.copyWith(
                                          color: locator<AppConfig>()
                                              .color()
                                              .primaryAccent,
                                          fontWeight: medium,
                                          fontSize: 12,
                                        ),
                                      ),
                                    ],
                                  ),
                                ),
                              ),
                            ),
                          );
                        } else {
                          return const SizedBox();
                        }
                      },
                    ),
                  ],
                ),
              ),
            ),
          ),
        ),
      ),
    );
  }
}
