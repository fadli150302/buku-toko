import 'dart:math';

import 'package:equatable/equatable.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

import '../../../data/model/my_model.dart';
import '../widget/transcation_goods_item.dart';

part 'add_goods_in_transaction_state.dart';

class AddGoodsInTransactionCubit extends Cubit<AddGoodsInTransactionState> {
  AddGoodsInTransactionCubit(this.listGoods)
      : super(const AddGoodsInTransactionInitial([]));

  List<TransactionGoodsItem> listGoods;
  setListGoods(List<TransactionGoodsItem> list) {
    listGoods = list;
    emit(AddGoodsInTransactionSuccess(listGoods));
  }

  getListGoods() {
    emit(AddGoodsInTransactionSuccess(listGoods));
  }

  addGoods(TransactionGoodsItem item) {
    listGoods.add(item);
    emit(AddGoodsInTransactionSuccess(listGoods));
  }

  removeGoods(Goods goods) {
    listGoods.removeWhere((element) => element.goods.id == goods.id);
    emit(AddGoodsInTransactionSuccess(listGoods));
  }
}
