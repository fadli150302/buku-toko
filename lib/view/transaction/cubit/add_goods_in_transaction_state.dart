part of 'add_goods_in_transaction_cubit.dart';

abstract class AddGoodsInTransactionState extends Equatable {
  const AddGoodsInTransactionState(this.listGoods);
  final List<TransactionGoodsItem> listGoods;
  @override
  List<Object> get props => [];
}

class AddGoodsInTransactionInitial extends AddGoodsInTransactionState {
  const AddGoodsInTransactionInitial(super.listGoods);
}

class AddGoodsInTransactionSuccess extends AddGoodsInTransactionState {
  const AddGoodsInTransactionSuccess(super.listGoods);
  @override
  List<Object> get props => [Random().nextDouble()];
}
