import 'package:buku_toko/data/model/my_model.dart';
import 'package:buku_toko/theme/color.dart';
import 'package:buku_toko/theme/textstyle.dart';
import 'package:buku_toko/widget/pop_widget.dart';
import 'package:flutter/cupertino.dart';

import '../../../util/app_config.dart';
import '../../../util/const.dart';
import '../../../util/date.dart';
import '../../../util/locator.dart';
import '../../../util/route.dart';
import '../add_transaction_page.dart';

class TransactionItem extends StatelessWidget {
  const TransactionItem({required this.transaction, super.key});
  final Transaction transaction;
  @override
  Widget build(BuildContext context) {
    return PopWidget(
      onTap: () {
        locator<NavigationService>().navigatorKey.currentState?.pushNamed(
              AddTransactionPage.route,
              arguments: transaction,
            );
      },
      child: Container(
        height: 74,
        margin: const EdgeInsets.only(bottom: 12),
        padding: const EdgeInsets.symmetric(vertical: 12, horizontal: 16),
        decoration: BoxDecoration(
          color: white,
          borderRadius: BorderRadius.circular(15),
        ),
        child: Row(
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          children: [
            Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                Text(
                  'Rp ${idNumberFormat.format(transaction.totalPrice)}',
                  style: poppinsTextStyle.copyWith(
                    color: locator<AppConfig>().color().primary,
                    fontWeight: semiBold,
                    fontSize: 14,
                  ),
                ),
                const SizedBox(
                  height: 6,
                ),
                Text(
                  'Total Item :  ${transaction.totalItem}',
                  style: poppinsTextStyle.copyWith(
                    color: locator<AppConfig>().color().primaryAccent,
                    fontWeight: medium,
                    fontSize: 14,
                  ),
                ),
              ],
            ),
            Column(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              crossAxisAlignment: CrossAxisAlignment.end,
              children: [
                Text(
                  'Profit : Rp ${idNumberFormat.format(transaction.totalProfit)}',
                  style: poppinsTextStyle.copyWith(
                    color: successMain,
                    fontWeight: reguler,
                    fontSize: 14,
                  ),
                ),
                Row(
                  children: [
                    Text(
                      locator<MyDate>()
                          .getDateWithInputStringIndonesiaFormatJustDate(
                        transaction.createdAt,
                      ),
                      style: poppinsTextStyle.copyWith(
                        color: locator<AppConfig>().color().primaryAccent,
                        fontWeight: medium,
                        fontSize: 14,
                      ),
                    ),
                  ],
                ),
              ],
            ),
          ],
        ),
      ),
    );
  }
}
