import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

import '../../../data/model/my_model.dart';
import '../../../theme/theme.dart';
import '../../../util/app_config.dart';
import '../../../util/const.dart';
import '../../../util/locator.dart';
import '../cubit/add_goods_in_transaction_cubit.dart';

class TransactionGoodsItem extends StatelessWidget {
  const TransactionGoodsItem({
    required this.goods,
    required this.total,
    required this.callback,
    required this.isTransactionClear,
    super.key,
  });
  final Goods goods;
  final ValueNotifier<int> total;
  final VoidCallback callback;
  final bool isTransactionClear;
  @override
  Widget build(BuildContext context) {
    return Container(
      height: isTransactionClear ? 76 : 86,
      margin: const EdgeInsets.only(bottom: 12),
      padding: const EdgeInsets.symmetric(vertical: 12, horizontal: 16),
      decoration: BoxDecoration(
        color: white,
        borderRadius: BorderRadius.circular(15),
      ),
      child: Row(
        mainAxisAlignment: MainAxisAlignment.spaceBetween,
        children: [
          Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              Text(
                goods.name,
                style: poppinsTextStyle.copyWith(
                  color: locator<AppConfig>().color().primary,
                  fontWeight: semiBold,
                  fontSize: 14,
                ),
              ),
              if (!isTransactionClear)
                const SizedBox(
                  height: 4,
                ),
              if (!isTransactionClear)
                Text(
                  'Stock : ${goods.stock}',
                  style: poppinsTextStyle.copyWith(
                    color: locator<AppConfig>().color().primaryAccent,
                    fontWeight: reguler,
                    fontSize: 13,
                  ),
                ),
            ],
          ),
          Column(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            crossAxisAlignment: CrossAxisAlignment.end,
            children: [
              Row(
                children: [
                  Text(
                    'Rp${idNumberFormat.format(goods.sellPrice)}',
                    style: poppinsTextStyle.copyWith(
                      color: successMain,
                      fontWeight: medium,
                      fontSize: 14,
                    ),
                  ),
                  Text(
                    '/${goods.typeGoods}',
                    style: poppinsTextStyle.copyWith(
                      color: locator<AppConfig>().color().primaryAccent,
                      fontWeight: reguler,
                      fontSize: 14,
                    ),
                  ),
                ],
              ),
              Row(
                children: [
                  if (!isTransactionClear)
                    GestureDetector(
                      onTap: () {
                        if (total.value == 1) {
                          callback();
                        } else {
                          total.value--;
                          BlocProvider.of<AddGoodsInTransactionCubit>(context)
                              .getListGoods();
                        }
                      },
                      child: const Icon(
                        Icons.remove_circle_outline_rounded,
                        color: black26,
                      ),
                    ),
                  if (!isTransactionClear)
                    const SizedBox(
                      width: 12,
                    ),
                  ValueListenableBuilder(
                    valueListenable: total,
                    builder: (context, value, child) {
                      if (isTransactionClear) {
                        return Text(
                          'Total : ${value.toString()}',
                          style: poppinsTextStyle.copyWith(
                            color: locator<AppConfig>().color().primary,
                            fontWeight: reguler,
                            fontSize: 14,
                          ),
                        );
                      }
                      return Text(
                        value.toString(),
                        style: poppinsTextStyle.copyWith(
                          color: locator<AppConfig>().color().primary,
                          fontWeight: reguler,
                          fontSize: 14,
                        ),
                      );
                    },
                  ),
                  if (!isTransactionClear)
                    const SizedBox(
                      width: 12,
                    ),
                  if (!isTransactionClear)
                    GestureDetector(
                      onTap: () {
                        if (total.value < goods.stock) {
                          total.value++;
                          BlocProvider.of<AddGoodsInTransactionCubit>(context)
                              .getListGoods();
                        }
                      },
                      child: const Icon(
                        Icons.add_circle_outline_rounded,
                        color: black26,
                      ),
                    ),
                ],
              ),
            ],
          ),
        ],
      ),
    );
  }
}
