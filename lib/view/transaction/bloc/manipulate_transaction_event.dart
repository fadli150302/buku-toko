part of 'manipulate_transaction_bloc.dart';

abstract class ManipulateTransactionEvent extends Equatable {
  const ManipulateTransactionEvent();

  @override
  List<Object> get props => [];
}

class AddTransactionEvent extends ManipulateTransactionEvent {
  final int guestPay;
  final int totalItem;
  final int totalPrice;
  final int totalProfit;
  final Map<Goods, int> map;
  const AddTransactionEvent({
    required this.guestPay,
    required this.map,
    required this.totalItem,
    required this.totalProfit,
    required this.totalPrice,
  });
}

class DeleteTransactionEvent extends ManipulateTransactionEvent {
  final Transaction transaction;
  const DeleteTransactionEvent(this.transaction);
}
