import 'package:buku_toko/data/model/my_model.dart';
import 'package:buku_toko/data/repository/finance_repository.dart';
import 'package:buku_toko/data/repository/goods_repository.dart';
import 'package:buku_toko/data/repository/transaction_repository.dart';
import 'package:equatable/equatable.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

part 'manipulate_transaction_event.dart';
part 'manipulate_transaction_state.dart';

class ManipulateTransactionBloc
    extends Bloc<ManipulateTransactionEvent, ManipulateTransactionState> {
  final TransactionRepository _transactionRepository;
  final GoodsRepository _goodsRepository;
  final FinanceRepository _financeRepository;
  ManipulateTransactionBloc(
    this._goodsRepository,
    this._financeRepository,
    this._transactionRepository,
  ) : super(ManipulateTransactionInitial()) {
    on<AddTransactionEvent>((event, emit) async {
      try {
        emit(ManipulateTransactionLoading());
        final now = DateTime.now();
        final transactionId = now.microsecondsSinceEpoch;
        final financeId = transactionId + 1;

        List<String> listGoods = [];

        for (var e in event.map.entries) {
          await _goodsRepository.reduceStockGoods(e.value, e.key.id);
          listGoods.add(e.key.toTransactionModel(e.value));
        }

        // UPDATE FINANCE
        await _financeRepository.addFinance(Finance(
          id: financeId,
          cash: event.totalPrice,
          createdAt: now.toString(),
          isIn: true,
          updatedAt: now.toString(),
          description: 'Penjualan Dengan Total ${event.totalItem} barang',
        ));

        // ADD TRANSACTION
        await _transactionRepository.addTransaction(Transaction(
          id: transactionId,
          financeId: financeId,
          createdAt: now.toString(),
          guestPay: event.guestPay,
          totalItem: event.totalItem,
          totalPrice: event.totalPrice,
          listGoods: listGoods,
          totalProfit: event.totalProfit,
          updatedAt: now.toString(),
        ));

        emit(ManipulateTransactionSuccess());
      } catch (e) {
        emit(const ManipulateTransactionError('Error Happen'));
      }
    });

    on<DeleteTransactionEvent>((event, emit) async {
      try {
        emit(ManipulateTransactionLoading());
        // GET OLD GOODS
        final List<Goods> oldGoods = [];
        for (var element in event.transaction.listGoods) {
          oldGoods.add(Goods.fromTransactionModel(element));
        }

        // RESTORE STOCK GOODS
        for (var element in oldGoods) {
          await _goodsRepository.addStockGoods(element.stock, element.id);
        }

        // UPDATE FINANCE
        await _financeRepository.deleteFinance(event.transaction.financeId);

        // UPDATE TRANSACTION
        await _transactionRepository.deleteTransaction(event.transaction.id);
        emit(ManipulateTransactionSuccess());
      } catch (e) {
        emit(const ManipulateTransactionError('Error Happen'));
      }
    });
  }
}
