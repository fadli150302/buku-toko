part of 'manipulate_transaction_bloc.dart';

abstract class ManipulateTransactionState extends Equatable {
  const ManipulateTransactionState();

  @override
  List<Object> get props => [];
}

class ManipulateTransactionInitial extends ManipulateTransactionState {}

class ManipulateTransactionSuccess extends ManipulateTransactionState {}

class ManipulateTransactionError extends ManipulateTransactionState {
  final String message;
  const ManipulateTransactionError(this.message);
}

class ManipulateTransactionLoading extends ManipulateTransactionState {}
