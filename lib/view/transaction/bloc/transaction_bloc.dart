import 'package:buku_toko/data/model/my_model.dart';
import 'package:equatable/equatable.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

import '../../../data/repository/transaction_repository.dart';

part 'transaction_event.dart';
part 'transaction_state.dart';

class TransactionBloc extends Bloc<TransactionEvent, TransactionState> {
  final TransactionRepository _transactionRepository;
  TransactionBloc(this._transactionRepository) : super(TransactionInitial()) {
    on<GetTransactionEvent>((event, emit) async {
      try {
        emit(TransactionLoading());
        final listGoods = await _transactionRepository.getAllTransaction();
        emit(TransactionSuccess(listGoods));
      } catch (e) {
        emit(const TransactionError('Error Happen'));
      }
    });
  }
}
