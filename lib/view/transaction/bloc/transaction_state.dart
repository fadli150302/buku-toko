part of 'transaction_bloc.dart';

abstract class TransactionState extends Equatable {
  const TransactionState();

  @override
  List<Object> get props => [];
}

class TransactionInitial extends TransactionState {}

class TransactionSuccess extends TransactionState {
  final List<Transaction> listGoods;
  const TransactionSuccess(this.listGoods);
}

class TransactionError extends TransactionState {
  final String message;
  const TransactionError(this.message);
}

class TransactionLoading extends TransactionState {}
