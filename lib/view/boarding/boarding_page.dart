import 'package:buku_toko/view/home/home_page.dart';
import 'package:flutter/material.dart';

import '../../util/locator.dart';
import '../../util/route.dart';

class BoardingPage extends StatelessWidget {
  const BoardingPage({super.key});
  static const route = '/boarding_page';
  @override
  Widget build(BuildContext context) {
    return Center(
      child: TextButton(
        onPressed: () {
          locator<NavigationService>().navigateTo(HomePage.route);
        },
        child: const Text('Next'),
      ),
    );
  }
}
