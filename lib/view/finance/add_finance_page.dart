import 'package:buku_toko/util/const.dart';
import 'package:buku_toko/util/loading.dart';
import 'package:buku_toko/util/toast.dart';
import 'package:buku_toko/view/finance/bloc/finance_bloc.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

import '../../data/model/my_model.dart';
import '../../theme/theme.dart';
import '../../util/app_config.dart';
import '../../util/locator.dart';
import '../../widget/custom_container_widget.dart';
import '../../widget/pop_widget.dart';
import 'bloc/manipulate_finance_bloc.dart';

class AddFinancePage extends StatefulWidget {
  const AddFinancePage({this.finance, super.key});
  static const route = '/add_finance_page';
  final Finance? finance;

  @override
  State<AddFinancePage> createState() => _AddFinancePageState();
}

class _AddFinancePageState extends State<AddFinancePage> {
  TextEditingController totalMoneyController = TextEditingController();
  TextEditingController description = TextEditingController();
  final list = [enter, exit];
  ValueNotifier<String> isInNotifier = ValueNotifier(enter);

  @override
  void initState() {
    super.initState();
    totalMoneyController.text = widget.finance?.cash.toString() ?? empty;
    description.text = widget.finance?.description.toString() ?? empty;
    isInNotifier.value = (widget.finance?.isIn ?? false) == true ? enter : exit;
  }

  @override
  Widget build(BuildContext context) {
    return BlocListener<ManipulateFinanceBloc, ManipulateFinanceState>(
      listener: (context, state) {
        LoadingOverlay.dismiss();
        if (state is ManipulateFinanceSuccess) {
          BlocProvider.of<FinanceBloc>(context).add(GetFinanceEvent());
          Navigator.pop(context);
          Toast.showSuccess('Success', context);
        } else if (state is ManipulateFinanceError) {
          Toast.showError(state.message, context);
        } else {
          LoadingOverlay.show(context);
        }
      },
      child: Scaffold(
        backgroundColor: locator<AppConfig>().color().secondaryAccent,
        body: SizedBox(
          height: MediaQuery.of(context).size.height,
          width: MediaQuery.of(context).size.width,
          child: SingleChildScrollView(
            child: Padding(
              padding: const EdgeInsets.all(20),
              child: SafeArea(
                child: Column(
                  children: [
                    // HEADER
                    CustomContainerWidget(
                      containerType: CustomContainerType.container_312_x_119,
                      child: Padding(
                        padding: const EdgeInsets.all(20),
                        child: Column(
                          children: [
                            Row(
                              children: [
                                GestureDetector(
                                  onTap: () {
                                    Navigator.pop(context);
                                  },
                                  child: const Icon(
                                    Icons.arrow_back,
                                    color: white,
                                  ),
                                ),
                                const SizedBox(
                                  width: 16,
                                ),
                                Text(
                                  'Tambah Data Keuangan',
                                  style: poppinsTextStyle.copyWith(
                                    fontWeight: semiBold,
                                    fontSize: 16,
                                    color: white,
                                  ),
                                ),
                                const Spacer(),
                                widget.finance != null
                                    ? PopWidget(
                                        scale: 0.04,
                                        onTap: () {
                                          BlocProvider.of<
                                              ManipulateFinanceBloc>(
                                            context,
                                          ).add(DeleteFinanceEvent(
                                            widget.finance!,
                                          ));
                                        },
                                        child: iconDelete,
                                      )
                                    : PopWidget(
                                        onTap: () {},
                                        child: iconAdd,
                                      ),
                              ],
                            ),
                          ],
                        ),
                      ),
                    ),
                    // BODY
                    Padding(
                      padding: const EdgeInsets.symmetric(vertical: 14),
                      child: CustomContainerWidget(
                        containerType: CustomContainerType.container_312_x_445,
                        child: Padding(
                          padding: const EdgeInsets.symmetric(horizontal: 20),
                          child: Column(
                            crossAxisAlignment: CrossAxisAlignment.start,
                            children: [
                              const SizedBox(
                                height: 20,
                              ),
                              // JUMLAH UANG
                              Text(
                                'Jumlah Uang',
                                style: poppinsTextStyle.copyWith(
                                  color: white,
                                  fontWeight: medium,
                                  fontSize: 12,
                                ),
                              ),
                              const SizedBox(
                                height: 6,
                              ),
                              Container(
                                padding:
                                    const EdgeInsets.symmetric(horizontal: 20),
                                decoration: BoxDecoration(
                                  color: white,
                                  borderRadius: BorderRadius.circular(12),
                                ),
                                child: TextField(
                                  controller: totalMoneyController,
                                  style: poppinsTextStyle.copyWith(
                                    fontWeight: semiBold,
                                    fontSize: 14,
                                    color: locator<AppConfig>().color().primary,
                                  ),
                                  textAlign: TextAlign.start,
                                  decoration: InputDecoration(
                                    border: InputBorder.none,
                                    hintStyle: poppinsTextStyle.copyWith(
                                      fontWeight: semiBold,
                                      fontSize: 14,
                                      color: black12,
                                    ),
                                    prefixText: rp,
                                    hintText: 'Jumlah Uang',
                                  ),
                                  keyboardType: TextInputType.number,
                                ),
                              ),
                              const SizedBox(
                                height: 14,
                              ),
                              // DESKRIPSI
                              Text(
                                'Deskripsi',
                                style: poppinsTextStyle.copyWith(
                                  color: white,
                                  fontWeight: medium,
                                  fontSize: 12,
                                ),
                              ),
                              const SizedBox(
                                height: 6,
                              ),
                              Container(
                                padding:
                                    const EdgeInsets.symmetric(horizontal: 20),
                                decoration: BoxDecoration(
                                  color: white,
                                  borderRadius: BorderRadius.circular(12),
                                ),
                                child: TextField(
                                  controller: description,
                                  style: poppinsTextStyle.copyWith(
                                    fontWeight: medium,
                                    fontSize: 14,
                                    color: black54,
                                  ),
                                  textAlign: TextAlign.start,
                                  decoration: InputDecoration(
                                    border: InputBorder.none,
                                    hintStyle: poppinsTextStyle.copyWith(
                                      fontWeight: semiBold,
                                      fontSize: 14,
                                      color: black12,
                                    ),
                                    hintText:
                                        'Contoh : Modal Awal, Tambahan Dana',
                                  ),
                                ),
                              ),
                              const SizedBox(
                                height: 14,
                              ),
                              // UANG MASUK / KELUAR
                              Text(
                                'Apakah Uang Masuk/Keluar',
                                style: poppinsTextStyle.copyWith(
                                  color: white,
                                  fontWeight: medium,
                                  fontSize: 12,
                                ),
                              ),
                              const SizedBox(
                                height: 6,
                              ),
                              Container(
                                padding:
                                    const EdgeInsets.symmetric(horizontal: 20),
                                decoration: BoxDecoration(
                                  color: white,
                                  borderRadius: BorderRadius.circular(12),
                                ),
                                child: SizedBox(
                                  width: MediaQuery.of(context).size.width,
                                  child: ValueListenableBuilder(
                                    valueListenable: isInNotifier,
                                    builder: (context, value, child) {
                                      return DropdownButtonHideUnderline(
                                        child: DropdownButton<String>(
                                          borderRadius: const BorderRadius.all(
                                            Radius.circular(12),
                                          ),
                                          value: isInNotifier.value,
                                          icon: iconArrowDown,
                                          elevation: 2,
                                          style: poppinsTextStyle.copyWith(
                                            color: black54,
                                          ),
                                          onChanged: (String? value) {
                                            isInNotifier.value = value!;
                                          },
                                          items: list
                                              .map<DropdownMenuItem<String>>(
                                            (String value) {
                                              return DropdownMenuItem<String>(
                                                value: value,
                                                child: SizedBox(
                                                  child: Row(
                                                    children: [
                                                      SizedBox(
                                                        width: 36,
                                                        child: Align(
                                                          alignment: Alignment
                                                              .centerLeft,
                                                          child: value == enter
                                                              ? iconArrowUpRight
                                                              : iconArrowDownLeft,
                                                        ),
                                                      ),
                                                      Text(value),
                                                    ],
                                                  ),
                                                ),
                                              );
                                            },
                                          ).toList(),
                                        ),
                                      );
                                    },
                                  ),
                                ),
                              ),

                              const SizedBox(
                                height: 20,
                              ),
                            ],
                          ),
                        ),
                      ),
                    ),
                    // FOOTER
                    PopWidget(
                      scale: 0.02,
                      onTap: () {
                        final now = DateTime.now();
                        final mili = now.millisecondsSinceEpoch;
                        if (widget.finance != null) {
                          BlocProvider.of<ManipulateFinanceBloc>(context)
                              .add(EditFinanceEvent(
                            Finance(
                              id: widget.finance!.id,
                              cash:
                                  int.tryParse(totalMoneyController.text) ?? 0,
                              description: description.text,
                              isIn: isInNotifier.value == enter ? true : false,
                              createdAt: widget.finance!.createdAt,
                              updatedAt: now.toString(),
                            ),
                          ));
                        } else {
                          BlocProvider.of<ManipulateFinanceBloc>(context)
                              .add(AddFinanceEvent(
                            Finance(
                              id: mili,
                              cash:
                                  int.tryParse(totalMoneyController.text) ?? 0,
                              description: description.text,
                              isIn: isInNotifier.value == enter ? true : false,
                              createdAt: now.toString(),
                              updatedAt: now.toString(),
                            ),
                          ));
                        }
                      },
                      child: CustomContainerWidget(
                        containerType: CustomContainerType.container_312_x_67,
                        child: Padding(
                          padding: const EdgeInsets.all(15),
                          child: Container(
                            padding: const EdgeInsets.all(12),
                            decoration: const BoxDecoration(
                              color: white,
                              borderRadius:
                                  BorderRadius.all(Radius.circular(12)),
                            ),
                            child: Row(
                              mainAxisAlignment: MainAxisAlignment.center,
                              children: [
                                Text(
                                  'Simpan',
                                  style: poppinsTextStyle.copyWith(
                                    color: locator<AppConfig>()
                                        .color()
                                        .primaryAccent,
                                    fontWeight: medium,
                                    fontSize: 12,
                                  ),
                                ),
                              ],
                            ),
                          ),
                        ),
                      ),
                    ),
                  ],
                ),
              ),
            ),
          ),
        ),
      ),
    );
  }
}
