import 'package:buku_toko/theme/color.dart';
import 'package:buku_toko/theme/textstyle.dart';
import 'package:buku_toko/view/finance/add_finance_page.dart';
import 'package:buku_toko/view/finance/widget/finance_item.dart';
import 'package:buku_toko/widget/pop_widget.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

import '../../util/app_config.dart';
import '../../util/locator.dart';
import '../../util/route.dart';
import '../../widget/custom_container_widget.dart';
import 'bloc/finance_bloc.dart';

class ListFinancePage extends StatelessWidget {
  const ListFinancePage({super.key});
  static const route = '/list_finance_page';
  @override
  Widget build(BuildContext context) {
    BlocProvider.of<FinanceBloc>(context).add(GetFinanceEvent());
    return Scaffold(
      backgroundColor: locator<AppConfig>().color().secondaryAccent,
      body: SizedBox(
        height: MediaQuery.of(context).size.height,
        width: MediaQuery.of(context).size.width,
        child: Padding(
          padding: const EdgeInsets.all(20),
          child: SafeArea(
            child: Column(
              children: [
                // HEADER
                CustomContainerWidget(
                  containerType: CustomContainerType.container_312_x_119,
                  child: Padding(
                    padding: const EdgeInsets.all(20),
                    child: Column(
                      children: [
                        Row(
                          children: [
                            GestureDetector(
                              onTap: () {
                                Navigator.pop(context);
                              },
                              child: const Icon(
                                Icons.arrow_back,
                                color: white,
                              ),
                            ),
                            const SizedBox(
                              width: 16,
                            ),
                            Text(
                              'Data Keuangan',
                              style: poppinsTextStyle.copyWith(
                                fontWeight: semiBold,
                                fontSize: 16,
                                color: white,
                              ),
                            ),
                            const Spacer(),
                            PopWidget(
                              onTap: () {},
                              child: const Icon(
                                Icons.arrow_drop_down_rounded,
                                color: white,
                              ),
                            ),
                          ],
                        ),
                        const SizedBox(
                          height: 20,
                        ),
                        Container(
                          decoration: BoxDecoration(
                            color: white,
                            borderRadius: BorderRadius.circular(12),
                          ),
                          child: TextField(
                            style: poppinsTextStyle.copyWith(
                              fontWeight: semiBold,
                              fontSize: 12,
                              color: black26,
                            ),
                            textAlign: TextAlign.center,
                            decoration: InputDecoration(
                              border: InputBorder.none,
                              hintStyle: poppinsTextStyle.copyWith(
                                fontWeight: semiBold,
                                fontSize: 12,
                                color: black12,
                              ),
                              hintText: 'Search',
                            ),
                          ),
                        ),
                      ],
                    ),
                  ),
                ),
                // BODY
                Expanded(
                  child: Padding(
                    padding: const EdgeInsets.symmetric(vertical: 14),
                    child: CustomContainerWidget(
                      containerType: CustomContainerType.container_312_x_445,
                      child: BlocBuilder<FinanceBloc, FinanceState>(
                        builder: (context, state) {
                          if (state is FinanceSuccess) {
                            return ListView(
                              padding: const EdgeInsets.symmetric(
                                vertical: 14,
                                horizontal: 12,
                              ),
                              children: [
                                ...state.listFinance.map(
                                  (e) => FinanceItem(finance: e),
                                ),
                              ],
                            );
                          }
                          return const SizedBox();
                        },
                      ),
                    ),
                  ),
                ),
                // FOOTER
                PopWidget(
                  scale: 0.02,
                  onTap: () {
                    locator<NavigationService>()
                        .navigatorKey
                        .currentState
                        ?.pushNamed(
                          AddFinancePage.route,
                        );
                  },
                  child: CustomContainerWidget(
                    containerType: CustomContainerType.container_312_x_67,
                    child: Padding(
                      padding: const EdgeInsets.all(15),
                      child: Container(
                        padding: const EdgeInsets.all(12),
                        decoration: const BoxDecoration(
                          color: white,
                          borderRadius: BorderRadius.all(Radius.circular(12)),
                        ),
                        child: Row(
                          mainAxisAlignment: MainAxisAlignment.center,
                          children: [
                            Text(
                              'Tambah Data Keuangan',
                              style: poppinsTextStyle.copyWith(
                                color:
                                    locator<AppConfig>().color().primaryAccent,
                                fontWeight: medium,
                                fontSize: 12,
                              ),
                            ),
                          ],
                        ),
                      ),
                    ),
                  ),
                ),
              ],
            ),
          ),
        ),
      ),
    );
  }
}
