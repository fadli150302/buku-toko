import 'package:buku_toko/data/repository/finance_repository.dart';
import 'package:equatable/equatable.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

import '../../../data/model/my_model.dart';

part 'manipulate_finance_event.dart';
part 'manipulate_finance_state.dart';

class ManipulateFinanceBloc
    extends Bloc<ManipulateFinanceEvent, ManipulateFinanceState> {
  final FinanceRepository _financeRepository;
  ManipulateFinanceBloc(this._financeRepository)
      : super(ManipulateFinanceInitial()) {
    on<AddFinanceEvent>((event, emit) async {
      try {
        emit(ManipulateFinanceLoading());
        await _financeRepository.addFinance(event.finance);
        emit(ManipulateFinanceSuccess());
      } catch (e) {
        emit(const ManipulateFinanceError('Error Happen'));
      }
    });

    on<EditFinanceEvent>((event, emit) async {
      try {
        emit(ManipulateFinanceLoading());
        await _financeRepository.updateFinance(event.finance);
        emit(ManipulateFinanceSuccess());
      } catch (e) {
        emit(const ManipulateFinanceError('Error Happen'));
      }
    });

    on<DeleteFinanceEvent>((event, emit) async {
      try {
        emit(ManipulateFinanceLoading());
        await _financeRepository.deleteFinance(event.finance.id);
        emit(ManipulateFinanceSuccess());
      } catch (e) {
        emit(const ManipulateFinanceError('Error Happen'));
      }
    });
  }
}
