import 'package:equatable/equatable.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

import '../../../data/model/my_model.dart';
import '../../../data/repository/finance_repository.dart';

part 'finance_event.dart';
part 'finance_state.dart';

class FinanceBloc extends Bloc<FinanceEvent, FinanceState> {
  final FinanceRepository _financeRepository;
  FinanceBloc(this._financeRepository) : super(FinanceInitial()) {
    on<GetFinanceEvent>((event, emit) async {
      try {
        emit(FinanceLoading());
        final listFinance = await _financeRepository.getAllFinance();
        emit(FinanceSuccess(listFinance));
      } catch (e) {
        emit(const FinanceError('Error Happen'));
      }
    });
  }
}
