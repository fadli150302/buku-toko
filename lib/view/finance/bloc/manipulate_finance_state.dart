part of 'manipulate_finance_bloc.dart';

abstract class ManipulateFinanceState extends Equatable {
  const ManipulateFinanceState();

  @override
  List<Object> get props => [];
}

class ManipulateFinanceInitial extends ManipulateFinanceState {}

class ManipulateFinanceSuccess extends ManipulateFinanceState {}

class ManipulateFinanceError extends ManipulateFinanceState {
  final String message;
  const ManipulateFinanceError(this.message);
}

class ManipulateFinanceLoading extends ManipulateFinanceState {}
