part of 'manipulate_finance_bloc.dart';

abstract class ManipulateFinanceEvent extends Equatable {
  const ManipulateFinanceEvent();

  @override
  List<Object> get props => [];
}

class AddFinanceEvent extends ManipulateFinanceEvent {
  final Finance finance;
  const AddFinanceEvent(this.finance);
}

class EditFinanceEvent extends ManipulateFinanceEvent {
  final Finance finance;
  const EditFinanceEvent(this.finance);
}

class DeleteFinanceEvent extends ManipulateFinanceEvent {
  final Finance finance;
  const DeleteFinanceEvent(this.finance);
}
