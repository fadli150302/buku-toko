part of 'finance_bloc.dart';

abstract class FinanceState extends Equatable {
  const FinanceState();

  @override
  List<Object> get props => [];
}

class FinanceInitial extends FinanceState {}

class FinanceSuccess extends FinanceState {
  final List<Finance> listFinance;
  const FinanceSuccess(this.listFinance);
}

class FinanceError extends FinanceState {
  final String message;
  const FinanceError(this.message);
}

class FinanceLoading extends FinanceState {}
