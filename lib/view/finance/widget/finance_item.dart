import 'package:buku_toko/util/date.dart';
import 'package:buku_toko/widget/pop_widget.dart';
import 'package:flutter/cupertino.dart';

import '../../../data/model/my_model.dart';
import '../../../theme/theme.dart';
import '../../../util/app_config.dart';
import '../../../util/const.dart';
import '../../../util/locator.dart';
import '../../../util/route.dart';
import '../add_finance_page.dart';

class FinanceItem extends StatelessWidget {
  const FinanceItem({required this.finance, super.key});
  final Finance finance;
  @override
  Widget build(BuildContext context) {
    return PopWidget(
      onTap: () {
        locator<NavigationService>().navigatorKey.currentState?.pushNamed(
              AddFinancePage.route,
              arguments: finance,
            );
      },
      child: Container(
        margin: const EdgeInsets.only(bottom: 12),
        padding: const EdgeInsets.symmetric(vertical: 12, horizontal: 16),
        decoration: BoxDecoration(
          color: white,
          borderRadius: BorderRadius.circular(15),
        ),
        child: Column(
          children: [
            Row(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                finance.isIn ? iconArrowUpRight : iconArrowDownLeft,
                const SizedBox(
                  width: 6,
                ),
                Flexible(
                  child: Text(
                    finance.description,
                    style: poppinsTextStyle.copyWith(
                      color: locator<AppConfig>().color().primary,
                      fontWeight: semiBold,
                      overflow: TextOverflow.ellipsis,
                      fontSize: 14,
                    ),
                    maxLines: 2,
                  ),
                ),
              ],
            ),
            const SizedBox(
              height: 12,
            ),
            Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              crossAxisAlignment: CrossAxisAlignment.end,
              children: [
                Row(
                  children: [
                    Text(
                      'Rp ${idNumberFormat.format(finance.cash)}',
                      style: poppinsTextStyle.copyWith(
                        color: finance.isIn ? successMain : warningMain,
                        fontWeight: medium,
                        fontSize: 14,
                      ),
                    ),
                  ],
                ),
                Text(
                  locator<MyDate>()
                      .getDateWithInputStringIndonesiaFormatJustDate(
                    finance.createdAt,
                  ),
                  style: poppinsTextStyle.copyWith(
                    color: locator<AppConfig>().color().primaryAccent,
                    fontWeight: reguler,
                    fontSize: 12,
                  ),
                ),
              ],
            ),
          ],
        ),
      ),
    );
  }
}
