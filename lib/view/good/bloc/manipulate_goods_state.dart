part of 'manipulate_goods_bloc.dart';

abstract class ManipulateGoodsState extends Equatable {
  const ManipulateGoodsState();

  @override
  List<Object> get props => [];
}

class ManipulateGoodsInitial extends ManipulateGoodsState {}

class ManipulateGoodsSuccess extends ManipulateGoodsState {}

class ManipulateGoodsError extends ManipulateGoodsState {
  final String message;
  const ManipulateGoodsError(this.message);
}

class ManipulateGoodsLoading extends ManipulateGoodsState {}
