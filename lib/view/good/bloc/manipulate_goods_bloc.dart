import 'package:buku_toko/data/model/my_model.dart';
import 'package:buku_toko/data/repository/finance_repository.dart';
import 'package:buku_toko/data/repository/goods_repository.dart';
import 'package:equatable/equatable.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

part 'manipulate_goods_event.dart';
part 'manipulate_goods_state.dart';

class ManipulateGoodsBloc
    extends Bloc<ManipulateGoodsEvent, ManipulateGoodsState> {
  final GoodsRepository _goodsRepository;
  final FinanceRepository _financeRepository;
  ManipulateGoodsBloc(this._goodsRepository, this._financeRepository)
      : super(ManipulateGoodsInitial()) {
    on<AddGoodsEvent>((event, emit) async {
      try {
        final micro = DateTime.now().microsecondsSinceEpoch;
        emit(ManipulateGoodsLoading());
        await _goodsRepository.addGoods(event.goods);
        await _financeRepository.addFinance(Finance(
          cash: event.goods.buyPrice * event.goods.stock,
          createdAt: event.goods.createdAt,
          isIn: false,
          id: micro,
          updatedAt: event.goods.createdAt,
          description:
              'Penambahan Barang (${event.goods.name}) Sebanyak ${event.goods.stock}',
        ));
        emit(ManipulateGoodsSuccess());
      } catch (e) {
        emit(const ManipulateGoodsError('Error Happen'));
      }
    });

    on<EditGoodsEvent>((event, emit) async {
      try {
        emit(ManipulateGoodsLoading());
        await _goodsRepository.updateGoods(event.goods);
        emit(ManipulateGoodsSuccess());
      } catch (e) {
        emit(const ManipulateGoodsError('Error Happen'));
      }
    });

    on<DeleteGoodsEvent>((event, emit) async {
      try {
        emit(ManipulateGoodsLoading());
        await _goodsRepository.deleteGoods(event.goods.id);
        emit(ManipulateGoodsSuccess());
      } catch (e) {
        emit(const ManipulateGoodsError('Error Happen'));
      }
    });
  }
}
