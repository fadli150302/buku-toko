part of 'manipulate_goods_bloc.dart';

abstract class ManipulateGoodsEvent extends Equatable {
  const ManipulateGoodsEvent();

  @override
  List<Object> get props => [];
}

class AddGoodsEvent extends ManipulateGoodsEvent {
  final Goods goods;
  const AddGoodsEvent(this.goods);
}

class EditGoodsEvent extends ManipulateGoodsEvent {
  final Goods goods;
  const EditGoodsEvent(this.goods);
}

class DeleteGoodsEvent extends ManipulateGoodsEvent {
  final Goods goods;
  const DeleteGoodsEvent(this.goods);
}
