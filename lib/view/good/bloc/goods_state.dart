part of 'goods_bloc.dart';

abstract class GoodsState extends Equatable {
  const GoodsState();

  @override
  List<Object> get props => [];
}

class GoodsInitial extends GoodsState {}

class GoodsSuccess extends GoodsState {
  final List<Goods> listGoods;
  const GoodsSuccess(this.listGoods);
}

class GoodsError extends GoodsState {
  final String message;
  const GoodsError(this.message);
}

class GoodsLoading extends GoodsState {}
