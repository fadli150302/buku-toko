import 'package:buku_toko/data/model/my_model.dart';
import 'package:buku_toko/data/repository/goods_repository.dart';
import 'package:equatable/equatable.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

part 'goods_event.dart';
part 'goods_state.dart';

class GoodsBloc extends Bloc<GoodsEvent, GoodsState> {
  final GoodsRepository _goodsRepository;
  GoodsBloc(this._goodsRepository) : super(GoodsInitial()) {
    on<GetGoodsEvent>((event, emit) async {
      try {
        emit(GoodsLoading());
        final listGoods = await _goodsRepository.getAllGoods();
        emit(GoodsSuccess(listGoods));
      } catch (e) {
        emit(const GoodsError('Error Happen'));
      }
    });
  }
}
