part of 'goods_bloc.dart';

abstract class GoodsEvent extends Equatable {
  const GoodsEvent();

  @override
  List<Object> get props => [];
}

class GetGoodsEvent extends GoodsEvent {}
