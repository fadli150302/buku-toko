import 'package:buku_toko/theme/color.dart';
import 'package:buku_toko/theme/textstyle.dart';
import 'package:buku_toko/view/good/add_goods_page.dart';
import 'package:buku_toko/view/good/widget/goods_item.dart';
import 'package:buku_toko/widget/pop_widget.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

import '../../util/app_config.dart';
import '../../util/locator.dart';
import '../../util/route.dart';
import '../../widget/custom_container_widget.dart';
import 'bloc/goods_bloc.dart';

class ListGoodsPage extends StatelessWidget {
  const ListGoodsPage({required this.isInTranscation, super.key});
  final bool isInTranscation;
  static const route = '/list_goods_page';
  @override
  Widget build(BuildContext context) {
    BlocProvider.of<GoodsBloc>(context).add(GetGoodsEvent());
    return Scaffold(
      backgroundColor: locator<AppConfig>().color().secondaryAccent,
      body: SizedBox(
        height: MediaQuery.of(context).size.height,
        width: MediaQuery.of(context).size.width,
        child: Padding(
          padding: const EdgeInsets.all(20),
          child: SafeArea(
            child: Column(
              children: [
                // HEADER
                CustomContainerWidget(
                  containerType: CustomContainerType.container_312_x_119,
                  child: Padding(
                    padding: const EdgeInsets.all(20),
                    child: Column(
                      children: [
                        Row(
                          children: [
                            GestureDetector(
                              onTap: () {
                                Navigator.pop(context);
                              },
                              child: const Icon(
                                Icons.arrow_back,
                                color: white,
                              ),
                            ),
                            const SizedBox(
                              width: 16,
                            ),
                            Text(
                              'Daftar Barang',
                              style: poppinsTextStyle.copyWith(
                                fontWeight: semiBold,
                                fontSize: 16,
                                color: white,
                              ),
                            ),
                            const Spacer(),
                            PopWidget(
                              onTap: () {},
                              child: const Icon(
                                Icons.arrow_drop_down_rounded,
                                color: white,
                              ),
                            ),
                          ],
                        ),
                        const SizedBox(
                          height: 20,
                        ),
                        Container(
                          decoration: BoxDecoration(
                            color: white,
                            borderRadius: BorderRadius.circular(12),
                          ),
                          child: TextField(
                            style: poppinsTextStyle.copyWith(
                              fontWeight: semiBold,
                              fontSize: 12,
                              color: black26,
                            ),
                            textAlign: TextAlign.center,
                            decoration: InputDecoration(
                              border: InputBorder.none,
                              hintStyle: poppinsTextStyle.copyWith(
                                fontWeight: semiBold,
                                fontSize: 12,
                                color: black12,
                              ),
                              hintText: 'Search',
                            ),
                          ),
                        ),
                      ],
                    ),
                  ),
                ),
                // BODY
                Expanded(
                  child: Padding(
                    padding: const EdgeInsets.symmetric(vertical: 14),
                    child: CustomContainerWidget(
                      containerType: CustomContainerType.container_312_x_445,
                      child: BlocBuilder<GoodsBloc, GoodsState>(
                        builder: (context, state) {
                          if (state is GoodsSuccess) {
                            return ListView(
                              padding: const EdgeInsets.symmetric(
                                vertical: 14,
                                horizontal: 12,
                              ),
                              children: [
                                ...state.listGoods.map(
                                  (e) => GoodsItem(
                                    goods: e,
                                    callback: () {
                                      if (isInTranscation) {
                                        Navigator.pop(context, e);
                                      } else {
                                        locator<NavigationService>()
                                            .navigatorKey
                                            .currentState
                                            ?.pushNamed(
                                              AddGoodsPage.route,
                                              arguments: e,
                                            );
                                      }
                                    },
                                  ),
                                ),
                              ],
                            );
                          }
                          return const SizedBox();
                        },
                      ),
                    ),
                  ),
                ),
                // FOOTER
                PopWidget(
                  scale: 0.02,
                  onTap: () {
                    locator<NavigationService>()
                        .navigatorKey
                        .currentState
                        ?.pushNamed(
                          AddGoodsPage.route,
                        );
                  },
                  child: CustomContainerWidget(
                    containerType: CustomContainerType.container_312_x_67,
                    child: Padding(
                      padding: const EdgeInsets.all(15),
                      child: Container(
                        padding: const EdgeInsets.all(12),
                        decoration: const BoxDecoration(
                          color: white,
                          borderRadius: BorderRadius.all(Radius.circular(12)),
                        ),
                        child: Row(
                          mainAxisAlignment: MainAxisAlignment.center,
                          children: [
                            Text(
                              'Tambah Barang',
                              style: poppinsTextStyle.copyWith(
                                color:
                                    locator<AppConfig>().color().primaryAccent,
                                fontWeight: medium,
                                fontSize: 12,
                              ),
                            ),
                          ],
                        ),
                      ),
                    ),
                  ),
                ),
              ],
            ),
          ),
        ),
      ),
    );
  }
}
