import 'package:buku_toko/data/model/my_model.dart';
import 'package:buku_toko/theme/color.dart';
import 'package:buku_toko/theme/textstyle.dart';
import 'package:buku_toko/widget/pop_widget.dart';
import 'package:flutter/cupertino.dart';

import '../../../util/app_config.dart';
import '../../../util/const.dart';
import '../../../util/locator.dart';

class GoodsItem extends StatelessWidget {
  const GoodsItem({required this.goods, required this.callback, super.key});
  final Goods goods;
  final VoidCallback callback;
  @override
  Widget build(BuildContext context) {
    return PopWidget(
      onTap: callback,
      child: Container(
        height: 74,
        margin: const EdgeInsets.only(bottom: 12),
        padding: const EdgeInsets.symmetric(vertical: 12, horizontal: 16),
        decoration: BoxDecoration(
          color: white,
          borderRadius: BorderRadius.circular(15),
        ),
        child: Row(
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          children: [
            Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                Text(
                  goods.name,
                  style: poppinsTextStyle.copyWith(
                    color: locator<AppConfig>().color().primary,
                    fontWeight: semiBold,
                    fontSize: 14,
                  ),
                ),
                Text(
                  '${goods.stock} ${goods.typeGoods}',
                  style: poppinsTextStyle.copyWith(
                    color: locator<AppConfig>().color().primaryAccent,
                    fontWeight: reguler,
                    fontSize: 12,
                  ),
                ),
              ],
            ),
            Column(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              crossAxisAlignment: CrossAxisAlignment.end,
              children: [
                Text(
                  goods.id.toString(),
                  style: poppinsTextStyle.copyWith(
                    color: locator<AppConfig>().color().primaryAccent,
                    fontWeight: reguler,
                    fontSize: 12,
                  ),
                ),
                Row(
                  children: [
                    Text(
                      'Rp${idNumberFormat.format(goods.sellPrice)}',
                      style: poppinsTextStyle.copyWith(
                        color: successMain,
                        fontWeight: medium,
                        fontSize: 14,
                      ),
                    ),
                    Text(
                      '/${goods.typeGoods}',
                      style: poppinsTextStyle.copyWith(
                        color: locator<AppConfig>().color().primaryAccent,
                        fontWeight: reguler,
                        fontSize: 14,
                      ),
                    ),
                  ],
                ),
              ],
            ),
          ],
        ),
      ),
    );
  }
}
