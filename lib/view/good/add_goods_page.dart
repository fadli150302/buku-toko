import 'package:buku_toko/data/model/my_model.dart';
import 'package:buku_toko/util/loading.dart';
import 'package:buku_toko/util/toast.dart';
import 'package:buku_toko/view/good/bloc/goods_bloc.dart';
import 'package:buku_toko/view/good/bloc/manipulate_goods_bloc.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

import '../../theme/theme.dart';
import '../../util/app_config.dart';
import '../../util/const.dart';
import '../../util/locator.dart';
import '../../widget/custom_container_widget.dart';
import '../../widget/pop_widget.dart';

class AddGoodsPage extends StatefulWidget {
  const AddGoodsPage({this.goods, super.key});
  static const route = '/add_goods_page';
  final Goods? goods;

  @override
  State<AddGoodsPage> createState() => _AddGoodsPageState();
}

class _AddGoodsPageState extends State<AddGoodsPage> {
  TextEditingController nameController = TextEditingController();
  TextEditingController buyPriceController = TextEditingController();
  TextEditingController sellPriceController = TextEditingController();
  TextEditingController stockController = TextEditingController();
  TextEditingController goodTypeController = TextEditingController();

  @override
  void initState() {
    super.initState();
    nameController.text = widget.goods?.name ?? empty;
    buyPriceController.text = widget.goods?.buyPrice.toString() ?? empty;
    sellPriceController.text = widget.goods?.sellPrice.toString() ?? empty;
    stockController.text = widget.goods?.stock.toString() ?? empty;
    goodTypeController.text = widget.goods?.typeGoods ?? empty;
  }

  @override
  Widget build(BuildContext context) {
    return BlocListener<ManipulateGoodsBloc, ManipulateGoodsState>(
      listener: (context, state) {
        LoadingOverlay.dismiss();
        if (state is ManipulateGoodsSuccess) {
          BlocProvider.of<GoodsBloc>(context).add(GetGoodsEvent());
          Navigator.pop(context);
          Toast.showSuccess('Success', context);
        } else if (state is ManipulateGoodsError) {
          Toast.showError('Error', context);
        } else {
          LoadingOverlay.show(context);
        }
      },
      child: Scaffold(
        backgroundColor: locator<AppConfig>().color().secondaryAccent,
        body: SizedBox(
          height: MediaQuery.of(context).size.height,
          width: MediaQuery.of(context).size.width,
          child: SingleChildScrollView(
            child: Padding(
              padding: const EdgeInsets.all(20),
              child: SafeArea(
                child: Column(
                  children: [
                    // HEADER
                    CustomContainerWidget(
                      containerType: CustomContainerType.container_312_x_119,
                      child: Padding(
                        padding: const EdgeInsets.all(20),
                        child: Column(
                          children: [
                            Row(
                              children: [
                                GestureDetector(
                                  onTap: () {
                                    Navigator.pop(context);
                                  },
                                  child: const Icon(
                                    Icons.arrow_back,
                                    color: white,
                                  ),
                                ),
                                const SizedBox(
                                  width: 16,
                                ),
                                Text(
                                  widget.goods != null
                                      ? 'Edit Barang'
                                      : 'Tambah Barang',
                                  style: poppinsTextStyle.copyWith(
                                    fontWeight: semiBold,
                                    fontSize: 16,
                                    color: white,
                                  ),
                                ),
                                const Spacer(),
                                widget.goods != null
                                    ? PopWidget(
                                        scale: 0.04,
                                        onTap: () {
                                          BlocProvider.of<ManipulateGoodsBloc>(
                                            context,
                                          ).add(DeleteGoodsEvent(
                                            widget.goods!,
                                          ));
                                        },
                                        child: iconDelete,
                                      )
                                    : PopWidget(
                                        onTap: () {},
                                        child: iconAdd,
                                      ),
                              ],
                            ),
                          ],
                        ),
                      ),
                    ),
                    // BODY
                    Padding(
                      padding: const EdgeInsets.symmetric(vertical: 14),
                      child: CustomContainerWidget(
                        containerType: CustomContainerType.container_312_x_445,
                        child: Padding(
                          padding: const EdgeInsets.symmetric(horizontal: 20),
                          child: Column(
                            crossAxisAlignment: CrossAxisAlignment.start,
                            children: [
                              const SizedBox(
                                height: 20,
                              ),
                              // NAMA
                              Text(
                                'Nama Barang',
                                style: poppinsTextStyle.copyWith(
                                  color: white,
                                  fontWeight: medium,
                                  fontSize: 12,
                                ),
                              ),
                              const SizedBox(
                                height: 6,
                              ),
                              Container(
                                padding:
                                    const EdgeInsets.symmetric(horizontal: 20),
                                decoration: BoxDecoration(
                                  color: white,
                                  borderRadius: BorderRadius.circular(12),
                                ),
                                child: TextField(
                                  controller: nameController,
                                  style: poppinsTextStyle.copyWith(
                                    fontWeight: semiBold,
                                    fontSize: 14,
                                    color: locator<AppConfig>().color().primary,
                                  ),
                                  textAlign: TextAlign.start,
                                  decoration: InputDecoration(
                                    border: InputBorder.none,
                                    hintStyle: poppinsTextStyle.copyWith(
                                      fontWeight: semiBold,
                                      fontSize: 14,
                                      color: black12,
                                    ),
                                    hintText: 'Nama Barang',
                                  ),
                                ),
                              ),
                              const SizedBox(
                                height: 14,
                              ),
                              // MODAL
                              Text(
                                'Modal Per Barang',
                                style: poppinsTextStyle.copyWith(
                                  color: white,
                                  fontWeight: medium,
                                  fontSize: 12,
                                ),
                              ),
                              const SizedBox(
                                height: 6,
                              ),
                              Container(
                                padding:
                                    const EdgeInsets.symmetric(horizontal: 20),
                                decoration: BoxDecoration(
                                  color: white,
                                  borderRadius: BorderRadius.circular(12),
                                ),
                                child: TextField(
                                  controller: buyPriceController,
                                  style: poppinsTextStyle.copyWith(
                                    fontWeight: medium,
                                    fontSize: 18,
                                    color: warningMain,
                                  ),
                                  textAlign: TextAlign.end,
                                  decoration: InputDecoration(
                                    border: InputBorder.none,
                                    hintStyle: poppinsTextStyle.copyWith(
                                      fontWeight: semiBold,
                                      fontSize: 14,
                                      color: black12,
                                    ),
                                    hintText: 'Harga pokok',
                                  ),
                                ),
                              ),
                              const SizedBox(
                                height: 14,
                              ),
                              // HARGA JUAL
                              Text(
                                'Harga Jual Per Barang',
                                style: poppinsTextStyle.copyWith(
                                  color: white,
                                  fontWeight: medium,
                                  fontSize: 12,
                                ),
                              ),
                              const SizedBox(
                                height: 6,
                              ),
                              Container(
                                padding:
                                    const EdgeInsets.symmetric(horizontal: 20),
                                decoration: BoxDecoration(
                                  color: white,
                                  borderRadius: BorderRadius.circular(12),
                                ),
                                child: TextField(
                                  controller: sellPriceController,
                                  style: poppinsTextStyle.copyWith(
                                    fontWeight: medium,
                                    fontSize: 18,
                                    color: successMain,
                                  ),
                                  textAlign: TextAlign.end,
                                  decoration: InputDecoration(
                                    border: InputBorder.none,
                                    hintStyle: poppinsTextStyle.copyWith(
                                      fontWeight: semiBold,
                                      fontSize: 14,
                                      color: black12,
                                    ),
                                    hintText: 'Harga jual',
                                  ),
                                ),
                              ),
                              const SizedBox(
                                height: 14,
                              ),
                              // JUMLAH BARANG
                              Text(
                                'Jumlah Barang',
                                style: poppinsTextStyle.copyWith(
                                  color: white,
                                  fontWeight: medium,
                                  fontSize: 12,
                                ),
                              ),
                              const SizedBox(
                                height: 6,
                              ),
                              Container(
                                padding:
                                    const EdgeInsets.symmetric(horizontal: 20),
                                decoration: BoxDecoration(
                                  color: white,
                                  borderRadius: BorderRadius.circular(12),
                                ),
                                child: TextField(
                                  controller: stockController,
                                  style: poppinsTextStyle.copyWith(
                                    fontWeight: medium,
                                    fontSize: 18,
                                    color: locator<AppConfig>().color().primary,
                                  ),
                                  textAlign: TextAlign.end,
                                  decoration: InputDecoration(
                                    border: InputBorder.none,
                                    hintStyle: poppinsTextStyle.copyWith(
                                      fontWeight: semiBold,
                                      fontSize: 14,
                                      color: black12,
                                    ),
                                    hintText: 'Jumlah Barang',
                                  ),
                                ),
                              ),
                              const SizedBox(
                                height: 14,
                              ),
                              // SATUAN BARANG
                              Text(
                                'Satuan Barang',
                                style: poppinsTextStyle.copyWith(
                                  color: white,
                                  fontWeight: medium,
                                  fontSize: 12,
                                ),
                              ),
                              const SizedBox(
                                height: 6,
                              ),
                              Container(
                                padding:
                                    const EdgeInsets.symmetric(horizontal: 20),
                                decoration: BoxDecoration(
                                  color: white,
                                  borderRadius: BorderRadius.circular(12),
                                ),
                                child: TextField(
                                  controller: goodTypeController,
                                  style: poppinsTextStyle.copyWith(
                                    fontWeight: medium,
                                    fontSize: 18,
                                    color: locator<AppConfig>().color().primary,
                                  ),
                                  textAlign: TextAlign.start,
                                  decoration: InputDecoration(
                                    border: InputBorder.none,
                                    hintStyle: poppinsTextStyle.copyWith(
                                      fontWeight: semiBold,
                                      fontSize: 14,
                                      color: black12,
                                    ),
                                    hintText: 'Satuan Barang',
                                  ),
                                ),
                              ),
                              const SizedBox(
                                height: 20,
                              ),
                            ],
                          ),
                        ),
                      ),
                    ),
                    // FOOTER
                    PopWidget(
                      scale: 0.02,
                      onTap: () {
                        final now = DateTime.now();
                        final mili = now.millisecondsSinceEpoch;

                        if (widget.goods != null) {
                          BlocProvider.of<ManipulateGoodsBloc>(context)
                              .add(EditGoodsEvent(
                            Goods(
                              id: widget.goods!.id,
                              name: nameController.text,
                              barCode: widget.goods!.barCode,
                              buyPrice:
                                  int.tryParse(buyPriceController.text) ?? 0,
                              createdAt: widget.goods!.createdAt,
                              discount: 0,
                              sellPrice:
                                  int.tryParse(sellPriceController.text) ?? 0,
                              stock: int.tryParse(stockController.text) ?? 0,
                              typeGoods: goodTypeController.text,
                              updatedAt: now.toString(),
                            ),
                          ));
                        } else {
                          BlocProvider.of<ManipulateGoodsBloc>(context)
                              .add(AddGoodsEvent(
                            Goods(
                              id: mili,
                              name: nameController.text,
                              barCode: '----',
                              buyPrice:
                                  int.tryParse(buyPriceController.text) ?? 0,
                              createdAt: now.toString(),
                              discount: 0,
                              sellPrice:
                                  int.tryParse(sellPriceController.text) ?? 0,
                              stock: int.tryParse(stockController.text) ?? 0,
                              typeGoods: goodTypeController.text,
                              updatedAt: now.toString(),
                            ),
                          ));
                        }
                      },
                      child: CustomContainerWidget(
                        containerType: CustomContainerType.container_312_x_67,
                        child: Padding(
                          padding: const EdgeInsets.all(15),
                          child: Container(
                            padding: const EdgeInsets.all(12),
                            decoration: const BoxDecoration(
                              color: white,
                              borderRadius:
                                  BorderRadius.all(Radius.circular(12)),
                            ),
                            child: Row(
                              mainAxisAlignment: MainAxisAlignment.center,
                              children: [
                                Text(
                                  'Simpan',
                                  style: poppinsTextStyle.copyWith(
                                    color: locator<AppConfig>()
                                        .color()
                                        .primaryAccent,
                                    fontWeight: medium,
                                    fontSize: 12,
                                  ),
                                ),
                              ],
                            ),
                          ),
                        ),
                      ),
                    ),
                  ],
                ),
              ),
            ),
          ),
        ),
      ),
    );
  }
}
