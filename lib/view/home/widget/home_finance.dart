import 'package:flutter/material.dart';

import '../../../theme/theme.dart';
import '../../../widget/custom_container_widget.dart';

class HomeFinance extends StatelessWidget {
  const HomeFinance({super.key});

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.only(
        top: 14,
        right: horizontalMargin,
        left: horizontalMargin,
      ),
      child: CustomContainerWidget(
        containerType: CustomContainerType.container_312_x_67,
        child: Padding(
          padding: const EdgeInsets.all(22),
          child: Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: [
              Text(
                'Uang kas : ',
                style: poppinsTextStyle.copyWith(
                  color: white,
                  fontSize: 12,
                  fontWeight: medium,
                ),
              ),
              Text(
                'Rp 500.000',
                style: poppinsTextStyle.copyWith(
                  color: white,
                  fontSize: 16,
                  fontWeight: semiBold,
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }
}
