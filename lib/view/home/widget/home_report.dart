import 'package:buku_toko/widget/custom_container_widget.dart';
import 'package:flutter/material.dart';

import '../../../theme/theme.dart';

class HomeReport extends StatelessWidget {
  const HomeReport({super.key});

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.only(
        top: 14,
        right: horizontalMargin,
        left: horizontalMargin,
      ),
      child: Row(
        children: [
          Expanded(
            child: CustomContainerWidget(
              containerType: CustomContainerType.container_234_x_334,
              child: Padding(
                padding:
                    const EdgeInsets.symmetric(horizontal: horizontalMargin),
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    const SizedBox(
                      height: 48,
                    ),
                    Text(
                      'Laporan Keuangan',
                      style: poppinsTextStyle.copyWith(
                        fontWeight: semiBold,
                        color: white,
                        fontSize: 18,
                      ),
                    ),
                    const SizedBox(
                      height: 4,
                    ),
                    Text(
                      '19 Maret 2022 - 21 Maret 2022',
                      style: poppinsTextStyle.copyWith(
                        fontWeight: light,
                        color: white,
                        fontSize: 10,
                      ),
                    ),
                    const SizedBox(
                      height: 26,
                    ),
                    Text(
                      'Pendapatan',
                      style: poppinsTextStyle.copyWith(
                        fontWeight: medium,
                        color: white,
                        fontSize: 12,
                      ),
                    ),
                    const SizedBox(
                      height: 4,
                    ),
                    Container(
                      width: MediaQuery.of(context).size.width,
                      height: 40,
                      padding: const EdgeInsets.only(right: 14),
                      decoration: BoxDecoration(
                        color: white,
                        borderRadius: BorderRadius.circular(10),
                      ),
                      child: Align(
                        alignment: Alignment.centerRight,
                        child: Text(
                          'Rp50.000.000',
                          style: poppinsTextStyle.copyWith(
                            color: successMain,
                            fontWeight: medium,
                            fontSize: 16,
                          ),
                        ),
                      ),
                    ),
                    const SizedBox(
                      height: 12,
                    ),
                    Text(
                      'Pengeluaran',
                      style: poppinsTextStyle.copyWith(
                        fontWeight: medium,
                        color: white,
                        fontSize: 12,
                      ),
                    ),
                    const SizedBox(
                      height: 4,
                    ),
                    Container(
                      width: MediaQuery.of(context).size.width,
                      height: 40,
                      padding: const EdgeInsets.only(right: 14),
                      decoration: BoxDecoration(
                        color: white,
                        borderRadius: BorderRadius.circular(10),
                      ),
                      child: Align(
                        alignment: Alignment.centerRight,
                        child: Text(
                          'Rp50.000.000',
                          style: poppinsTextStyle.copyWith(
                            color: warningMain,
                            fontWeight: medium,
                            fontSize: 16,
                          ),
                        ),
                      ),
                    ),
                    const SizedBox(
                      height: 12,
                    ),
                    Text(
                      'Keuntungan',
                      style: poppinsTextStyle.copyWith(
                        fontWeight: medium,
                        color: white,
                        fontSize: 12,
                      ),
                    ),
                    const SizedBox(
                      height: 4,
                    ),
                    Container(
                      width: MediaQuery.of(context).size.width,
                      height: 40,
                      padding: const EdgeInsets.only(right: 14),
                      decoration: BoxDecoration(
                        color: white,
                        borderRadius: BorderRadius.circular(10),
                      ),
                      child: Align(
                        alignment: Alignment.centerRight,
                        child: Text(
                          'Rp50.000.000',
                          style: poppinsTextStyle.copyWith(
                            color: primary1,
                            fontWeight: medium,
                            fontSize: 16,
                          ),
                        ),
                      ),
                    ),
                    const SizedBox(
                      height: 30,
                    ),
                  ],
                ),
              ),
            ),
          ),
          const SizedBox(
            width: 14,
          ),
          CustomContainerWidget(
            containerType: CustomContainerType.container_64_x_334,
            child: SizedBox(
              width: 64,
              child: Column(
                children: [
                  const SizedBox(
                    height: 60,
                  ),
                  const Icon(
                    Icons.punch_clock,
                    color: white,
                    size: 24,
                  ),
                  const SizedBox(
                    height: 34,
                  ),
                  Container(
                    height: 40,
                    width: 40,
                    decoration: const BoxDecoration(
                      color: white_40percent,
                      shape: BoxShape.circle,
                    ),
                    child: Center(
                      child: Text(
                        'This\nDay',
                        textAlign: TextAlign.center,
                        style: poppinsTextStyle.copyWith(
                          color: white,
                          fontWeight: semiBold,
                          fontSize: 8,
                        ),
                      ),
                    ),
                  ),
                  const SizedBox(
                    height: 12,
                  ),
                  Container(
                    height: 40,
                    width: 40,
                    decoration: const BoxDecoration(
                      color: white_40percent,
                      shape: BoxShape.circle,
                    ),
                    child: Center(
                      child: Text(
                        'This\nMonth',
                        textAlign: TextAlign.center,
                        style: poppinsTextStyle.copyWith(
                          color: white,
                          fontWeight: semiBold,
                          fontSize: 8,
                        ),
                      ),
                    ),
                  ),
                  const SizedBox(
                    height: 12,
                  ),
                  Container(
                    height: 40,
                    width: 40,
                    decoration: const BoxDecoration(
                      color: white_40percent,
                      shape: BoxShape.circle,
                    ),
                    child: Center(
                      child: Text(
                        'All\nTime',
                        textAlign: TextAlign.center,
                        style: poppinsTextStyle.copyWith(
                          color: white,
                          fontWeight: semiBold,
                          fontSize: 8,
                        ),
                      ),
                    ),
                  ),
                  const SizedBox(
                    height: 12,
                  ),
                  Container(
                    height: 40,
                    width: 40,
                    decoration: const BoxDecoration(
                      color: white_40percent,
                      shape: BoxShape.circle,
                    ),
                    child: Center(
                      child: Text(
                        'Custom',
                        textAlign: TextAlign.center,
                        style: poppinsTextStyle.copyWith(
                          color: white,
                          fontWeight: semiBold,
                          fontSize: 8,
                        ),
                      ),
                    ),
                  ),
                  const SizedBox(
                    height: 46,
                  ),
                ],
              ),
            ),
          ),
        ],
      ),
    );
  }
}
