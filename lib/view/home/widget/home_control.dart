import 'package:buku_toko/util/app_config.dart';
import 'package:buku_toko/util/locator.dart';
import 'package:buku_toko/util/route.dart';
import 'package:buku_toko/view/finance/list_finance_page.dart';
import 'package:buku_toko/view/good/list_goods_page.dart';
import 'package:buku_toko/view/home/widget/home_main_button.dart';
import 'package:buku_toko/view/transaction/list_transaction_page.dart';
import 'package:flutter/material.dart';

import '../../../theme/theme.dart';

class HomeControl extends StatelessWidget {
  const HomeControl({super.key});

  @override
  Widget build(BuildContext context) {
    final width = MediaQuery.of(context).size.width;
    final widthButton = width / 3.7;
    return Padding(
      padding: const EdgeInsets.only(
        top: 20,
        right: horizontalMargin,
        left: horizontalMargin,
        bottom: 20,
      ),
      child: Stack(
        alignment: Alignment.center,
        children: [
          Column(
            children: [
              Row(
                children: [
                  Expanded(
                    child: HomeMainButtonWidget(
                      homeMainButtonType: HomeMainButtonType.top_left,
                      callback: () {
                        locator<NavigationService>()
                            .navigatorKey
                            .currentState
                            ?.pushNamed(
                              ListGoodsPage.route,
                              arguments: false,
                            );
                      },
                      child: Row(
                        mainAxisAlignment: MainAxisAlignment.start,
                        children: [
                          const SizedBox(
                            width: 12,
                          ),
                          Icon(
                            Icons.stacked_bar_chart,
                            color: locator<AppConfig>().color().primary,
                            size: 20,
                          ),
                          const SizedBox(
                            width: 12,
                          ),
                          Text(
                            'Kelola\nBarang',
                            style: poppinsTextStyle.copyWith(
                              color: locator<AppConfig>().color().primaryAccent,
                              fontWeight: semiBold,
                              fontSize: 10,
                            ),
                          ),
                        ],
                      ),
                    ),
                  ),
                  const SizedBox(
                    width: 4,
                  ),
                  Expanded(
                    child: HomeMainButtonWidget(
                      homeMainButtonType: HomeMainButtonType.top_right,
                      callback: () {
                        locator<NavigationService>()
                            .navigateTo(ListTransactionPage.route);
                      },
                      child: Row(
                        mainAxisAlignment: MainAxisAlignment.end,
                        children: [
                          Text(
                            'Kelola\nTransaksi',
                            style: poppinsTextStyle.copyWith(
                              color: locator<AppConfig>().color().primaryAccent,
                              fontWeight: semiBold,
                              fontSize: 10,
                            ),
                          ),
                          const SizedBox(
                            width: 12,
                          ),
                          Icon(
                            Icons.shopping_basket,
                            color: locator<AppConfig>().color().primary,
                            size: 20,
                          ),
                          const SizedBox(
                            width: 12,
                          ),
                        ],
                      ),
                    ),
                  ),
                ],
              ),
              const SizedBox(
                height: 5,
              ),
              Row(
                children: [
                  Expanded(
                    child: HomeMainButtonWidget(
                      homeMainButtonType: HomeMainButtonType.bottom_left,
                      callback: () {},
                      child: Row(
                        mainAxisAlignment: MainAxisAlignment.start,
                        children: [
                          const SizedBox(
                            width: 12,
                          ),
                          Icon(
                            Icons.account_balance,
                            color: locator<AppConfig>().color().primary,
                            size: 20,
                          ),
                          const SizedBox(
                            width: 12,
                          ),
                          Text(
                            'Kelola\nKaryawan',
                            style: poppinsTextStyle.copyWith(
                              color: locator<AppConfig>().color().primaryAccent,
                              fontWeight: semiBold,
                              fontSize: 10,
                            ),
                          ),
                        ],
                      ),
                    ),
                  ),
                  const SizedBox(
                    width: 4,
                  ),
                  Expanded(
                    child: HomeMainButtonWidget(
                      homeMainButtonType: HomeMainButtonType.bottom_right,
                      callback: () {
                        locator<NavigationService>()
                            .navigateTo(ListFinancePage.route);
                      },
                      child: Row(
                        mainAxisAlignment: MainAxisAlignment.end,
                        children: [
                          Text(
                            'Kelola\nKeungan',
                            style: poppinsTextStyle.copyWith(
                              color: locator<AppConfig>().color().primaryAccent,
                              fontWeight: semiBold,
                              fontSize: 10,
                            ),
                          ),
                          const SizedBox(
                            width: 12,
                          ),
                          Icon(
                            Icons.monetization_on_rounded,
                            color: locator<AppConfig>().color().primary,
                            size: 20,
                          ),
                          const SizedBox(
                            width: 12,
                          ),
                        ],
                      ),
                    ),
                  ),
                ],
              ),
            ],
          ),
          SizedBox(
            width: widthButton,
            child: HomeMainButtonWidget(
              homeMainButtonType: HomeMainButtonType.start,
              callback: () {},
              child: Text(
                'START',
                style: poppinsTextStyle.copyWith(
                  fontWeight: semiBold,
                  color: white,
                  fontSize: 14,
                ),
              ),
            ),
          ),
        ],
      ),
    );
  }
}
