// ignore_for_file: constant_identifier_names

import 'package:buku_toko/widget/pop_widget.dart';
import 'package:flutter/material.dart';

import '../../../util/app_config.dart';
import '../../../util/locator.dart';

class HomeMainButtonWidget extends StatelessWidget {
  const HomeMainButtonWidget({
    required this.child,
    required this.homeMainButtonType,
    required this.callback,
    super.key,
  });
  final Widget child;
  final HomeMainButtonType homeMainButtonType;
  final VoidCallback callback;
  @override
  Widget build(BuildContext context) {
    return PopWidget(
      onTap: callback,
      child: Stack(
        alignment: Alignment.center,
        children: [
          Image.asset(
            'assets/image/template/${locator<AppConfig>().themeType().name}/button_${homeMainButtonType.name}.png',
          ),
          child,
        ],
      ),
    );
  }
}

enum HomeMainButtonType {
  top_left,
  top_right,
  bottom_left,
  bottom_right,
  start
}
