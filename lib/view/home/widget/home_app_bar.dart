import 'package:buku_toko/theme/color.dart';
import 'package:buku_toko/theme/size.dart';
import 'package:buku_toko/theme/textstyle.dart';
import 'package:buku_toko/widget/custom_container_widget.dart';
import 'package:flutter/material.dart';

class HomeAppBar extends StatelessWidget {
  const HomeAppBar({super.key});

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.only(
        top: 30,
        right: horizontalMargin,
        left: horizontalMargin,
      ),
      child: CustomContainerWidget(
        containerType: CustomContainerType.container_312_x_67,
        child: Padding(
          padding: const EdgeInsets.all(22),
          child: Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: [
              Text(
                'Toko Saya',
                style: poppinsTextStyle.copyWith(
                  color: white,
                  fontSize: 16,
                  fontWeight: semiBold,
                ),
              ),
              IconButton(
                padding: EdgeInsets.zero,
                onPressed: () {},
                icon: const Icon(
                  Icons.person_outline_rounded,
                  size: 30,
                  color: white,
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }
}
