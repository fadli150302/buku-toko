import 'package:buku_toko/util/app_config.dart';
import 'package:buku_toko/util/locator.dart';
import 'package:buku_toko/view/home/widget/home_app_bar.dart';
import 'package:buku_toko/view/home/widget/home_control.dart';
import 'package:buku_toko/view/home/widget/home_finance.dart';
import 'package:buku_toko/view/home/widget/home_report.dart';
import 'package:flutter/material.dart';

class HomePage extends StatefulWidget {
  const HomePage({super.key});
  static const route = '/home_page';
  @override
  State<HomePage> createState() => _HomePageState();
}

class _HomePageState extends State<HomePage> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: locator<AppConfig>().color().secondaryAccent,
      body: SizedBox(
        height: MediaQuery.of(context).size.height,
        width: MediaQuery.of(context).size.width,
        child: ListView(
          children: const [
            HomeAppBar(),
            HomeReport(),
            HomeFinance(),
            HomeControl(),
          ],
        ),
      ),
    );
  }
}
