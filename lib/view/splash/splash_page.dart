import 'package:buku_toko/util/locator.dart';
import 'package:buku_toko/view/boarding/boarding_page.dart';
import 'package:buku_toko/view/home/home_page.dart';
import 'package:flutter/material.dart';
import 'package:shared_preferences/shared_preferences.dart';

import '../../util/const.dart';
import '../../util/route.dart';

class SplashPage extends StatefulWidget {
  const SplashPage({super.key});
  @override
  State<SplashPage> createState() => _SplashPageState();
}

class _SplashPageState extends State<SplashPage> {
  @override
  void initState() {
    super.initState();
    nextPage();
  }

  nextPage() async {
    final isFirstTime =
        locator<SharedPreferences>().getBool(firstTimeKey) ?? true;
    await Future.delayed(const Duration(seconds: 1));
    if (isFirstTime) {
      locator<SharedPreferences>().setBool(firstTimeKey, false);
      locator<NavigationService>().navigateTo(BoardingPage.route);
    } else {
      locator<NavigationService>().navigateTo(HomePage.route);
    }
  }

  gotoPage(String route) {
    locator<NavigationService>().navigateTo(route);
  }

  @override
  Widget build(BuildContext context) {
    return const Scaffold(
      body: Center(
        child: Text('SPLASH'),
      ),
    );
  }
}
