// ignore_for_file: constant_identifier_names

import 'package:buku_toko/util/app_config.dart';
import 'package:buku_toko/util/locator.dart';
import 'package:flutter/material.dart';

import '../theme/theme.dart';

class CustomContainerWidget extends StatelessWidget {
  const CustomContainerWidget({
    required this.child,
    required this.containerType,
    super.key,
  });
  final Widget child;
  final CustomContainerType containerType;
  @override
  Widget build(BuildContext context) {
    return Container(
      decoration: BoxDecoration(
        borderRadius: const BorderRadius.all(Radius.circular(12)),
        color: locator<AppConfig>().color().primary,
      ),
      child: Container(
        decoration: BoxDecoration(
          borderRadius: const BorderRadius.all(Radius.circular(12)),
          gradient: LinearGradient(
            colors: [
              locator<AppConfig>().color().secondary_60percent,
              transparent,
            ],
            begin: Alignment.topCenter,
            end: Alignment.bottomCenter,
          ),
          image: DecorationImage(
            fit: BoxFit.cover,
            image: AssetImage(
              'assets/image/template/${locator<AppConfig>().themeType().name}/${containerType.name}.png',
            ),
          ),
        ),
        child: child,
      ),
    );
  }
}

enum CustomContainerType {
  container_234_x_334,
  container_312_x_67,
  container_312_x_67_2,
  container_312_x_119,
  container_312_x_156,
  container_312_x_445,
  container_312_x_543,
  container_312_x_644,
  container_64_x_334
}
