import 'package:flutter/material.dart';

import '../theme/color.dart';
import '../theme/textstyle.dart';

class ToastAnimatedWidget extends StatefulWidget {
  const ToastAnimatedWidget({
    Key? key,
    required this.message,
    required this.background,
    required this.icon,
    this.iconColor,
    this.textStyle,
    required this.duration,
  }) : super(key: key);
  final Duration duration;
  final String message;
  final Color background;
  final IconData? icon;
  final TextStyle? textStyle;
  final Color? iconColor;
  @override
  State<ToastAnimatedWidget> createState() => _ToastWidgetState();
}

class _ToastWidgetState extends State<ToastAnimatedWidget>
    with SingleTickerProviderStateMixin {
  late AnimationController _animationController;
  late final int milliseconds;
  @override
  void initState() {
    super.initState();
    milliseconds = widget.duration.inMilliseconds;
    _animationController = AnimationController(
      vsync: this,
      duration: Duration(milliseconds: milliseconds ~/ 5),
    )..forward();
    dismiss();
  }

  dismiss() async {
    await Future.delayed(Duration(milliseconds: milliseconds * 3 ~/ 5));
    if (mounted) {
      await _animationController.reverse();
    }
  }

  @override
  void dispose() {
    _animationController.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Positioned(
      bottom: 50,
      child: AnimatedBuilder(
        animation: _animationController,
        builder: (context, child) {
          return Transform.scale(
            scale: _animationController.value,
            child: SizedBox(
              width: MediaQuery.of(context).size.width,
              child: Container(
                alignment: Alignment.center,
                width: MediaQuery.of(context).size.width,
                child: Container(
                  decoration: BoxDecoration(
                    color: widget.background,
                    borderRadius: const BorderRadius.all(Radius.circular(10)),
                  ),
                  margin: const EdgeInsets.symmetric(horizontal: 20),
                  padding: const EdgeInsets.fromLTRB(16, 10, 16, 10),
                  child: Row(
                    mainAxisSize: MainAxisSize.min,
                    children: [
                      widget.icon != null
                          ? Icon(
                              widget.icon,
                              color: widget.iconColor ?? semiWhite,
                            )
                          : Container(),
                      widget.icon != null
                          ? const SizedBox(
                              width: 11,
                            )
                          : Container(),
                      DefaultTextStyle(
                        style: widget.textStyle ??
                            poppinsTextStyle.copyWith(fontSize: 14),
                        child: Text(
                          widget.message,
                        ),
                      ),
                    ],
                  ),
                ),
              ),
            ),
          );
        },
      ),
    );
  }
}
