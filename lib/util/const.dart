import 'package:intl/intl.dart';

const firstTimeKey = 'FIRST_TIME_KEY';
const colorConfigKey = 'COLOR_CONFIG_KEY';
const themeTypeKey = 'THEME_TYPE_KEY';
const appName = 'Kasir Toko';
const rp = 'Rp';
const enter = 'Masuk';
const exit = 'Keluar';
const empty = '';
final idNumberFormat = NumberFormat('#,##0', 'id_ID');
