import 'dart:async';

class RequestLocal<T> {
  Future<T> requestLocal({
    required FutureOr<T> Function() computation,
    required String massage,
  }) async {
    try {
      return await computation();
    } catch (e) {
      return Future.error('$massage Failed');
    }
  }
}
