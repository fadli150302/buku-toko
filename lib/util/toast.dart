import 'package:buku_toko/theme/textstyle.dart';
import 'package:flutter/material.dart';
import 'package:motion_toast/motion_toast.dart';

import '../theme/color.dart';
import '../widget/toast_widget.dart';

class Toast {
  static void show(
    String msg,
    Color backgroundColor,
    BuildContext context, {
    IconData? icon,
    Duration duration = const Duration(milliseconds: 2500),
    Color? iconColor,
    TextStyle? textStyle,
  }) async {
    dismiss();
    Toast._createView(
      msg,
      context,
      backgroundColor,
      icon: icon,
      duration: duration,
      textStyle: textStyle,
      iconColor: iconColor,
    );
  }

  static void showSuccess(
    String msg,
    BuildContext context, {
    IconData? icon,
    Duration duration = const Duration(milliseconds: 2500),
  }) async {
    dismiss();
    Toast._createView(
      msg,
      context,
      successSurface,
      icon: icon,
      duration: duration,
      textStyle: poppinsTextStyle.copyWith(color: successMain),
    );
  }

  static void showInDevelopment(
    BuildContext context, {
    IconData? icon,
    Duration duration = const Duration(milliseconds: 2500),
  }) async {
    dismiss();
    // Toast._createView(Label.inDevelopment.get(), context, grey,
    //     icon: Icons.android_rounded, duration: duration);
    MotionToast.info(
      title: Text(
        'Info',
        style: poppinsTextStyle.copyWith(fontWeight: medium, color: black68),
      ),
      description: Padding(
        padding: const EdgeInsets.only(top: 2),
        child: Text(
          'Dalam Tahap Pengembangan',
          style: poppinsTextStyle.copyWith(fontWeight: light, color: black54),
        ),
      ),
    ).show(context);
  }

  static void showError(
    String msg,
    BuildContext context, {
    IconData? icon,
    Duration duration = const Duration(milliseconds: 3000),
  }) async {
    dismiss();
    // Toast._createView(msg, context, warningSurface, icon: icon, duration: duration,textStyle: sofiaTextStyle.copyWith(color: warningMain));
    MotionToast.error(
      title: Text(
        'Failed',
        style: poppinsTextStyle.copyWith(fontWeight: medium, color: black68),
      ),
      description: Padding(
        padding: const EdgeInsets.only(top: 2),
        child: Text(
          msg,
          maxLines: 2,
          style: poppinsTextStyle.copyWith(
            fontWeight: light,
            color: black54,
            overflow: TextOverflow.ellipsis,
          ),
        ),
      ),
    ).show(context);
  }

  static void showWarning(
    String msg,
    BuildContext context, {
    IconData? icon,
    Duration duration = const Duration(milliseconds: 3000),
  }) async {
    dismiss();
    // Toast._createView(msg, context, warningSurface, icon: icon, duration: duration,textStyle: sofiaTextStyle.copyWith(color: warningMain));
    MotionToast.warning(
      title: Text(
        'Warning',
        style: poppinsTextStyle.copyWith(fontWeight: medium, color: black68),
      ),
      description: Padding(
        padding: const EdgeInsets.only(top: 2),
        child: Text(
          msg,
          style: poppinsTextStyle.copyWith(fontWeight: light, color: black54),
        ),
      ),
    ).show(context);
  }

  static OverlayEntry? _overlayEntry;
  static bool isVisible = false;

  static void _createView(
    String message,
    BuildContext context,
    Color background, {
    IconData? icon,
    required Duration duration,
    Color? iconColor,
    TextStyle? textStyle,
  }) async {
    var overlayState = Overlay.of(context);

    _overlayEntry = OverlayEntry(
      builder: (BuildContext context) => ToastAnimatedWidget(
        duration: duration,
        message: message,
        textStyle: textStyle,
        iconColor: iconColor,
        icon: icon,
        background: background,
      ),
    );
    isVisible = true;
    overlayState!.insert(_overlayEntry!);
  }

  static dismiss() async {
    if (!isVisible) {
      return;
    }
    isVisible = false;
    _overlayEntry!.remove();
  }
}
