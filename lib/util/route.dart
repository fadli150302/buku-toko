import 'package:buku_toko/data/repository/goods_repository.dart';
import 'package:buku_toko/util/locator.dart';
import 'package:buku_toko/view/boarding/boarding_page.dart';
import 'package:buku_toko/view/finance/bloc/manipulate_finance_bloc.dart';
import 'package:buku_toko/view/good/add_goods_page.dart';
import 'package:buku_toko/view/good/list_goods_page.dart';
import 'package:buku_toko/view/home/home_page.dart';
import 'package:buku_toko/view/splash/splash_page.dart';
import 'package:buku_toko/view/transaction/list_transaction_page.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

import '../data/model/my_model.dart';
import '../data/repository/finance_repository.dart';
import '../data/repository/transaction_repository.dart';
import '../view/finance/add_finance_page.dart';
import '../view/finance/list_finance_page.dart';
import '../view/good/bloc/manipulate_goods_bloc.dart';
import '../view/transaction/add_transaction_page.dart';
import '../view/transaction/bloc/manipulate_transaction_bloc.dart';
import '../view/transaction/cubit/add_goods_in_transaction_cubit.dart';

class NavigationService {
  final GlobalKey<NavigatorState> navigatorKey = GlobalKey<NavigatorState>();

  void navigateTo(String routeName) {
    navigatorKey.currentState?.pushNamed(routeName);
  }
}

class AppRouter {
  Route navigateToSplashPage() {
    return MaterialPageRoute(
      builder: (BuildContext context) {
        return const SplashPage();
      },
    );
  }

  Route navigateToHomePage() {
    return MaterialPageRoute(
      settings: const RouteSettings(name: HomePage.route),
      builder: (BuildContext context) {
        return const HomePage();
      },
    );
  }

  Route navigateToListGoodsPage(bool isInTranscation) {
    return MaterialPageRoute(
      settings: const RouteSettings(name: ListGoodsPage.route),
      builder: (BuildContext context) {
        return ListGoodsPage(
          isInTranscation: isInTranscation,
        );
      },
    );
  }

  Route navigateToAddGoodsPage(Goods? goods) {
    return MaterialPageRoute(
      settings: const RouteSettings(name: AddGoodsPage.route),
      builder: (BuildContext context) {
        return BlocProvider(
          create: (context) => ManipulateGoodsBloc(
            locator<GoodsRepositoryImpl>(),
            locator<FinanceRepositoryImpl>(),
          ),
          child: AddGoodsPage(
            goods: goods,
          ),
        );
      },
    );
  }

  Route navigateToListFinancePage() {
    return MaterialPageRoute(
      settings: const RouteSettings(name: ListFinancePage.route),
      builder: (BuildContext context) {
        return const ListFinancePage();
      },
    );
  }

  Route navigateToAddFinancePage(Finance? finance) {
    return MaterialPageRoute(
      settings: const RouteSettings(name: AddFinancePage.route),
      builder: (BuildContext context) {
        return BlocProvider(
          create: (context) =>
              ManipulateFinanceBloc(locator<FinanceRepositoryImpl>()),
          child: AddFinancePage(
            finance: finance,
          ),
        );
      },
    );
  }

  Route navigateToListTransactionPage() {
    return MaterialPageRoute(
      settings: const RouteSettings(name: ListTransactionPage.route),
      builder: (BuildContext context) {
        return const ListTransactionPage();
      },
    );
  }

  Route navigateToAddTransactionPage(Transaction? transaction) {
    return MaterialPageRoute(
      settings: const RouteSettings(name: AddTransactionPage.route),
      builder: (BuildContext context) {
        return MultiBlocProvider(
          providers: [
            BlocProvider(
              create: (context) => ManipulateTransactionBloc(
                locator<GoodsRepositoryImpl>(),
                locator<FinanceRepositoryImpl>(),
                locator<TransactionRepositoryImpl>(),
              ),
            ),
            BlocProvider(
              create: (context) => AddGoodsInTransactionCubit([]),
            ),
          ],
          child: AddTransactionPage(
            transaction: transaction,
          ),
        );
      },
    );
  }

  Route navigateToBoardingPage() {
    return MaterialPageRoute(
      settings: const RouteSettings(name: BoardingPage.route),
      builder: (BuildContext context) {
        return const BoardingPage();
      },
    );
  }

  Route? routes(RouteSettings settings) {
    switch (settings.name) {
      case '/':
        return navigateToSplashPage();
      case HomePage.route:
        return navigateToHomePage();
      case BoardingPage.route:
        return navigateToBoardingPage();
      case ListGoodsPage.route:
        return navigateToListGoodsPage(settings.arguments as bool);
      case AddGoodsPage.route:
        return navigateToAddGoodsPage(settings.arguments as Goods?);
      case ListFinancePage.route:
        return navigateToListFinancePage();
      case AddFinancePage.route:
        return navigateToAddFinancePage(settings.arguments as Finance?);
      case ListTransactionPage.route:
        return navigateToListTransactionPage();
      case AddTransactionPage.route:
        return navigateToAddTransactionPage(settings.arguments as Transaction?);
      default:
        return null;
    }
  }
}
