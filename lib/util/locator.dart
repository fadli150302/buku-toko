import 'package:buku_toko/data/repository/goods_repository.dart';
import 'package:buku_toko/data/source/local/goods_local_data_source.dart';
import 'package:buku_toko/theme/color.dart';
import 'package:buku_toko/theme/type_theme.dart';
import 'package:buku_toko/util/app_config.dart';
import 'package:buku_toko/util/date.dart';
import 'package:flutter_secure_storage/flutter_secure_storage.dart';
import 'package:get_it/get_it.dart';
import 'package:internet_connection_checker/internet_connection_checker.dart';
import 'package:shared_preferences/shared_preferences.dart';

import '../data/repository/finance_repository.dart';
import '../data/repository/transaction_repository.dart';
import '../data/source/local/finance_local_data_source.dart';
import '../data/source/local/transaction_local_data_source_m.dart';
import '../service/Database/database.dart';
import 'network_info.dart';
import 'route.dart';

final locator = GetIt.instance;

Future<void> initServiceLocator() async {
  final pref = await SharedPreferences.getInstance();
  locator.registerSingleton<SharedPreferences>(pref);
  locator.registerSingleton(NetworkInfoImpl(InternetConnectionChecker()));
  locator.registerSingleton(const FlutterSecureStorage());
  locator.registerSingleton(NavigationService());
  locator.registerSingleton(ObjectBoxDatabase());
  locator.registerSingleton(MyDate());
  locator.registerSingleton(GoodsRepositoryImpl(GoodsLocalDataSource()));
  locator.registerSingleton(FinanceRepositoryImpl(FinanceLocalDataSource()));
  locator.registerSingleton(
    TransactionRepositoryImpl(TransactionLocalDataSource()),
  );
  locator.registerSingleton(
    AppConfig(getAppColorBaseOnType(getColorType()), getThemeType()),
  );
}
