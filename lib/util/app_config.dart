import 'package:buku_toko/theme/color.dart';
import 'package:buku_toko/theme/type_theme.dart';

class AppConfig {
  AppColor _appColor;
  ThemeType _themeType;
  AppConfig(this._appColor, this._themeType);

  void changeColor(AppColor color) {
    _appColor = color;
  }

  void changeThemeType(ThemeType themeType) {
    _themeType = themeType;
  }

  ThemeType themeType() {
    return _themeType;
  }

  AppColor color() {
    return _appColor;
  }
}
