import 'package:flutter/cupertino.dart';

import '../../objectbox.g.dart';

abstract class Database {
  Future<void> initDatabase();
  Future<T?> getData<T>(int id);
  Future<List<T>> getAll<T>();
  Future<void> createData<T>(T entity);
  Future<void> deleteData<T>(int id);
  Future<void> updateData<T>(T entity, int id);
}

class ObjectBoxDatabase extends Database {
  Store? store;

  ObjectBoxDatabase() {
    initDatabase();
  }

  @override
  Future<void> initDatabase() async {
    try {
      store = await openStore();
    } catch (e) {
      debugPrint('DB Already Open');
    }
  }

  @override
  Future<void> createData<T>(T entity) async {
    store?.box<T>().put(entity);
  }

  @override
  Future<void> deleteData<T>(int id) async {
    store?.box<T>().remove(id);
  }

  @override
  Future<List<T>> getAll<T>() async {
    if (store != null) {
      return store!.box<T>().getAll();
    } else {
      return [];
    }
  }

  @override
  Future<T?> getData<T>(int id) async {
    return store?.box<T>().get(id);
  }

  @override
  Future<void> updateData<T>(T entity, int id) async {
    try {
      store?.box<T>().remove(id);
    } catch (e) {
      // DO NOTHING
    } finally {
      store?.box<T>().put(entity);
    }
  }
}
