import 'package:flutter/material.dart';
import 'package:google_fonts/google_fonts.dart';

final poppinsTextStyle = GoogleFonts.poppins();

const FontWeight light = FontWeight.w300;
const FontWeight reguler = FontWeight.w400;
const FontWeight medium = FontWeight.w500;
const FontWeight semiBold = FontWeight.w600;
const FontWeight bold = FontWeight.w700;
const FontWeight extraBold = FontWeight.w800;
