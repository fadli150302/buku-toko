import 'package:flutter/material.dart';

import 'color.dart';

const shadowCard =
    BoxShadow(color: shadowGrey, blurRadius: 1, offset: Offset(0, 1));
