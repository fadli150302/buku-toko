import 'package:buku_toko/theme/color.dart';
import 'package:flutter/material.dart';

const iconArrowDown = Icon(
  Icons.arrow_drop_down,
);
const iconArrowUpRight = RotationTransition(
  turns: AlwaysStoppedAnimation(45 / 360),
  child: Icon(
    Icons.arrow_upward_rounded,
    color: successMain,
  ),
);

const iconArrowDownLeft = RotationTransition(
  turns: AlwaysStoppedAnimation(45 / 360),
  child: Icon(
    Icons.arrow_downward_rounded,
    color: warningMain,
  ),
);

const iconDelete = Icon(
  Icons.delete,
  color: white,
);

const iconAdd = Icon(
  Icons.add,
  color: white,
);
