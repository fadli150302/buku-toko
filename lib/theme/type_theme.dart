import 'package:buku_toko/util/locator.dart';
import 'package:shared_preferences/shared_preferences.dart';

import '../util/const.dart';

enum ThemeType { standart, premium }

ThemeType getThemeType() {
  String? themeType = locator<SharedPreferences>().getString(themeTypeKey);
  if (themeType == null) {
    locator<SharedPreferences>()
        .setString(themeTypeKey, ThemeType.standart.name);
    themeType = ThemeType.standart.name;
  }
  for (var element in ThemeType.values) {
    if (element.name == themeType) {
      return element;
    }
  }
  return ThemeType.standart;
}
