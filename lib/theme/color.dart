import 'package:buku_toko/util/locator.dart';
import 'package:flutter/material.dart';
import 'package:shared_preferences/shared_preferences.dart';

import '../util/const.dart';

// BLACK
const Color shadowGrey = Color(0x24656565);
const Color black12 = Colors.black12;
const Color black26 = Colors.black26;
const Color black38 = Colors.black38;
const Color black54 = Colors.black54;
const Color black68 = Color.fromARGB(173, 0, 0, 0);
const Color black87 = Colors.black87;
const Color background = Color(0xFF0e1015);
const Color black = Color(0xFF000000);

// WHITE
const Color transparent = Color(0x00FFFFFF);
const Color semiWhite = Color(0xFFFAFAFA);
const Color white = Color(0xFFFFFFFF);
const Color white_40percent = Color.fromARGB(102, 255, 255, 255);

// MAIN COLOR 1
const primary1 = Color(0xFF001AFF);
const primaryAccent1 = Color(0xFF597EFF);
const secondary1 = Color(0xFF00F0FF);
const secondary1_60percent = Color(0x9900F0FF);
const secondaryAccent1 = Color(0xFFE0F9FF);

// MAIN COLOR 2
const primary2 = Color(0xFF001AFF);
const primaryAccent2 = Color(0xFF597EFF);
const secondary2 = Color(0xFF00F0FF);
const secondary2_60percent = Color(0x9900F0FF);
const secondaryAccent2 = Color(0xFFE0F9FF);

// STATUS COLOR
const warningMain = Color(0xFFFB5656);
const warningSurface = Color(0xFFFEDDDD);
const warningBorder = Color(0xFFFEC7C7);
const warningHover = Color(0xFFD14848);
const warningPressed = Color(0xFF7D2B2B);

const dangerMain = Color(0xFFF8B40D);
const dangerSurface = Color(0xFFFEF0CF);
const dangerBorder = Color(0xFFFDE6AE);
const dangerHover = Color(0xFFCF960B);
const dangerPressed = Color(0xFF7C5A06);

const infoMain = Color(0xFF4A76FC);
const infoSurface = Color(0xFFDBE4FE);
const infoBorder = Color(0xFFC3D1FE);
const infoHover = Color(0xFF3E62D2);
const infoPressed = Color(0xFF253B7E);

const successMain = Color(0xFF4DE25C);
const successSurface = Color(0xFFDBF9DE);
const successBorder = Color(0xFFC4F5C9);
const successHover = Color(0xFF40BC4D);
const successPressed = Color(0xFF26712E);

// COLOR SETTING
class AppColor {
  final Color primary;
  final Color primaryAccent;
  final Color secondary;
  final Color secondary_60percent;
  final Color secondaryAccent;

  AppColor({
    required this.primary,
    required this.primaryAccent,
    required this.secondary,
    required this.secondary_60percent,
    required this.secondaryAccent,
  });
}

getAppColorBaseOnType(int type) {
  switch (type) {
    case 1:
      return AppColor(
        primary: primary1,
        primaryAccent: primaryAccent1,
        secondary: secondary1,
        secondary_60percent: secondary1_60percent,
        secondaryAccent: secondaryAccent1,
      );
    case 2:
      return AppColor(
        primary: primary2,
        primaryAccent: primaryAccent2,
        secondary: secondary2,
        secondary_60percent: secondary2_60percent,
        secondaryAccent: secondaryAccent2,
      );

    default:
      return AppColor(
        primary: primary1,
        primaryAccent: primaryAccent1,
        secondary: secondary1,
        secondary_60percent: secondary1_60percent,
        secondaryAccent: secondaryAccent1,
      );
  }
}

getColorType() {
  int? colorType = locator<SharedPreferences>().getInt(colorConfigKey);
  if (colorType == null) {
    locator<SharedPreferences>().setInt(colorConfigKey, 1);
    colorType = 1;
  }
  return colorType;
}
